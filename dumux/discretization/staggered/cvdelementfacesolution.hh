// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::StaggeredFaceSolution
 */
#ifndef DUMUX_CVD_ELEMENT_FACE_SOLUTION_HH
#define DUMUX_CVD_ELEMENT_FACE_SOLUTION_HH

#include <algorithm>
#include <cassert>
#include <type_traits>
#include <vector>

namespace Dumux
{

/*!
 * \ingroup StaggeredDiscretization
 * \brief The global face variables class for staggered models
 */
template<class FaceSolutionVector>
class CVDElementFaceSolution
{
    using FacePrimaryVariables = std::decay_t<decltype(std::declval<FaceSolutionVector>()[0])>;

public:
    template <class FVGeometry, class Element>
    CVDElementFaceSolution(const FaceSolutionVector &sol, const Element& ele,  const FVGeometry &fvGeometry)
    {
        facePriVars_.clear();
        map_.clear();

        for (auto &&scvf : scvfs(fvGeometry))
        {
            int counter = 0;
            for (auto is : scvf.intersections())
            {
                if (is.inside() == ele)
                {
                    for (const auto dofJ : scvf.localVelVarsData()[counter].dofs)
                    {
                        if (std::find(map_.begin(), map_.end(), dofJ) == map_.end())
                        {
                            map_.push_back(dofJ);
                            facePriVars_.push_back(sol[dofJ]);
                            double factor = 1.;
                            if(!scvf.isSimple() && scvf.isInsideFiner())
                                factor = 0.5;
                            factorMap_.push_back(factor);
                        }
                    }
                    break;
                }
                counter++;
            }
        }
    }

    //! bracket operator const access
    template<typename IndexType>
    const FacePrimaryVariables& operator [](IndexType globalFaceDofIdx) const
    {
        const auto pos = std::find(map_.begin(), map_.end(), globalFaceDofIdx);
        assert (pos != map_.end());
        return facePriVars_[pos - map_.begin()];
    }

    //! bracket operator
    template<typename IndexType>
    FacePrimaryVariables& operator [](IndexType globalFaceDofIdx)
    {
        const auto pos = std::find(map_.begin(), map_.end(), globalFaceDofIdx);
        assert (pos != map_.end());
        return facePriVars_[pos - map_.begin()];
    }

    //! returns if that face solution exists
    template<typename IndexType>
    bool exists(IndexType globalFaceDofIdx)
    {
        const auto pos = std::find(map_.begin(), map_.end(), globalFaceDofIdx);
        if  (pos != map_.end())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    template<typename IndexType>
    double factor(IndexType globalFaceDofIdx) const
    {
        const auto pos = std::find(map_.begin(), map_.end(), globalFaceDofIdx);
        assert (pos != map_.end());
        return factorMap_[pos - map_.begin()];
    }

    std::vector<unsigned int> dofs() const
    {
        return map_;
    }

private:

    std::vector<FacePrimaryVariables> facePriVars_;
    std::vector<unsigned int> map_;
    std::vector<double> factorMap_;
};

} // end namespace

#endif
