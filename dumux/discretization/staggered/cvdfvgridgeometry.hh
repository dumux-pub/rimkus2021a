// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDStaggeredGridGeometry
 */
#ifndef DUMUX_COARSE_VEL_DISC_STAGGERED_FV_GRID_GEOMETRY
#define DUMUX_COARSE_VEL_DISC_STAGGERED_FV_GRID_GEOMETRY

#include <utility>

#include <dumux/discretization/basegridgeometry.hh>
#include <dumux/discretization/checkoverlapsize.hh>
#include <dumux/discretization/method.hh>
#include <unordered_map>

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Base class for cell center of face specific auxiliary FvGridGeometry classes.
 *        Provides a common interface and a pointer to the actual gridGeometry.
 */
template<class ActualGridGeometry>
class MyGridGeometryView
{
public:

    explicit MyGridGeometryView(const ActualGridGeometry* actualGridGeometry)
    : gridGeometry_(actualGridGeometry) {}

    //! export  the GridView type and the discretization method
    using GridView = typename ActualGridGeometry::GridView;
    static constexpr DiscretizationMethod discMethod = DiscretizationMethod::staggered;
    using LocalView = typename ActualGridGeometry::LocalView;

    /*!
     * \brief Returns true if this view if related to cell centered dofs
     */
    static constexpr bool isCellCenter() { return false; }

    /*!
     * \brief Returns true if this view if related to face dofs
     */
    static constexpr bool isFace() {return false; }

    /*!
     * \brief Return an integral constant index for cell centered dofs
     */
    static constexpr auto cellCenterIdx()
    { return typename ActualGridGeometry::DofTypeIndices::CellCenterIdx{}; }

    /*!
     * \brief Return an integral constant index for face dofs
     */
    static constexpr auto faceIdx()
    { return typename ActualGridGeometry::DofTypeIndices::FaceIdx{}; }

    /*!
     * \brief Return the gridView this grid geometry object lives on
     */
    const auto& gridView() const
    { return gridGeometry_->gridView(); }

    /*!
     * \brief Returns the connectivity map of which dofs have derivatives with respect
     *        to a given dof.
     */
    const auto& connectivityMap() const // TODO return correct map
    { return gridGeometry_->connectivityMap(); }

    /*!
     * \brief Returns the mapper for vertices to indices for possibly adaptive grids.
     */
    const auto& vertexMapper() const
    { return gridGeometry_->vertexMapper(); }

    /*!
     * \brief Returns the mapper for elements to indices for constant grids.
     */
    const auto& elementMapper() const
    { return gridGeometry_->elementMapper(); }

    /*!
     * \brief Returns the actual gridGeometry we are a restriction of
     */
    const ActualGridGeometry& actualGridGeometry() const
    { return *gridGeometry_; }

protected:
    const ActualGridGeometry* gridGeometry_;

};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Cell center specific auxiliary FvGridGeometry classes.
 *        Required for the Dumux multi-domain framework.
 */
template <class ActualGridGeometry>
class MyCellCenterFVGridGeometry : public MyGridGeometryView<ActualGridGeometry>
{
    using ParentType = MyGridGeometryView<ActualGridGeometry>;
public:

    using ParentType::ParentType;

    /*!
     * \brief Returns true because this view is related to cell centered dofs
     */
    static constexpr bool isCellCenter() { return true; }

    /*!
     * \brief The total number of cell centered dofs
     */
    std::size_t numDofs() const
    { return this->gridGeometry_->numCellCenterDofs(); }
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Face specific auxiliary FvGridGeometry classes.
 *        Required for the Dumux multi-domain framework.
 */
template <class ActualGridGeometry>
class MyFaceFVGridGeometry : public MyGridGeometryView<ActualGridGeometry>
{
    using ParentType = MyGridGeometryView<ActualGridGeometry>;
public:

    using ParentType::ParentType;

    /*!
     * \brief Returns true because this view is related to face dofs
     */
    static constexpr bool isFace() {return true; }

    /*!
     * \brief The total number of cell centered dofs
     */
    std::size_t numDofs() const
    { return this->gridGeometry_->numFaceDofs(); }
};



/*!
 * \ingroup StaggeredDiscretization
 * \brief Base class for the finite volume geometry vector for staggered models
 *        This builds up the sub control volumes and sub control volume faces
 *        for each element.
 */
template<class GridView,
         bool cachingEnabled,
         class Traits>
class CVDStaggeredGridGeometry;

/*!
 * \ingroup StaggeredDiscretization
 * \brief Base class for the finite volume geometry vector for staggered models
 *        This builds up the sub control volumes and sub control volume faces
 *        for each element. Specialization in case the FVElementGeometries are stored.
 */
template<class GV, class Traits>
class CVDStaggeredGridGeometry<GV, true, Traits>
: public BaseGridGeometry<GV, Traits>
{
    using ThisType = CVDStaggeredGridGeometry<GV, true, Traits>;
    using ParentType = BaseGridGeometry<GV, Traits>;
    using IndexType = typename GV::IndexSet::IndexType;
    using Element = typename GV::template Codim<0>::Entity;

    using IntersectionMapper = typename Traits::IntersectionMapper;
    using GeometryHelper = typename Traits::GeometryHelper;
    using ConnectivityMap = typename Traits::template ConnectivityMap<ThisType>;
    using FluxCorrectionGeometry = typename Traits::template FluxCorrectionGeometry<ThisType>;

    using GridIndexType = typename GV::IndexSet::IndexType;
    static constexpr int dim = GV::dimension;
    static constexpr int dimWorld = GV::dimensionworld;
    using Scalar = typename GV::ctype;
    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

public:
    //! export discretization method
    static constexpr DiscretizationMethod discMethod = DiscretizationMethod::staggered;

    //! export the type of the fv element geometry (the local view type)
    using LocalView = typename Traits::template LocalView<ThisType, true>;
    //! export the type of sub control volume
    using SubControlVolume = typename Traits::SubControlVolume;
    //! export the type of sub control volume
    using SubControlVolumeFace = typename Traits::SubControlVolumeFace;
    //! export the grid view type
    using GridView = GV;
    //! export the dof type indices
    using DofTypeIndices = typename Traits::DofTypeIndices;

    //! return a integral constant for cell center dofs
    static constexpr auto cellCenterIdx()
    { return typename DofTypeIndices::CellCenterIdx{}; }

    //! return a integral constant for face dofs
    static constexpr auto faceIdx()
    { return typename DofTypeIndices::FaceIdx{}; }

    using CellCenterFVGridGeometryType = MyCellCenterFVGridGeometry<ThisType>;
    using FaceFVGridGeometryType = MyFaceFVGridGeometry<ThisType>;

    using GridGeometryTuple = std::tuple< MyCellCenterFVGridGeometry<ThisType>, MyFaceFVGridGeometry<ThisType> >;

    //! Constructor
    CVDStaggeredGridGeometry(const GridView& gridView)
    : ParentType(gridView)
    {
        // Check if the overlap size is what we expect
        if (!CheckOverlapSize<DiscretizationMethod::staggered>::isValid(gridView))
            DUNE_THROW(Dune::InvalidStateException, "The satggered discretization method needs at least an overlap of 1 for parallel computations. "
                                                     << " Set the parameter \"Grid.Overlap\" in the input file.");
        if (hasParamInGroup("Discretization", "TvdApproach"))
            GeometryHelper::setOrder(2);

        update_();
    }

    //! The total number of sub control volumes
    std::size_t numScv() const
    {
        return scvs_.size();
    }

    //! The total number of sub control volume faces
    std::size_t numScvf() const
    {
        return scvfs_.size();
    }

    //! The total number of boundary sub control volume faces
    std::size_t numBoundaryScvf() const
    {
        return numBoundaryScvf_;
    }

    //! The total number of intersections
    std::size_t numIntersections() const
    {
        return numIntersections_;
    }

    //! the total number of dofs
    std::size_t numDofs() const
    { return numCellCenterDofs() + numFaceDofs(); }

    std::size_t numCellCenterDofs() const
    { return this->gridView().size(0); }

    std::size_t numFaceDofs() const
    { return numIntersections(); }

    //! update all fvElementGeometries (call this after grid adaption)
    void update(const GridView& gridView)
    {
        ParentType::update(gridView);
        update_();
    }

    //! update all fvElementGeometries (call this after grid adaption)
    void update(GridView&& gridView)
    {
        ParentType::update(std::move(gridView));
        update_();
    }

    //! update all fvElementGeometries (do this again after grid adaption)
    void update_()
    {
        // clear containers (necessary after grid refinement)
        scvs_.clear();
        scvfs_.clear();
        scvfIndicesOfScv_.clear();

        auto intersectionMapper_ = std::make_shared<IntersectionMapper>(this->gridView());
        intersectionMapper_->update(this->gridView());
        numIntersections_ = intersectionMapper_->numIntersections();


        // determine size of containers
        IndexType numScvs = this->gridView().size(0);
        IndexType numScvf = 0;
        //count intersections (scvfs) using intersection mapper
        for (const auto& element : elements(this->gridView()))
            numScvf += intersectionMapper_->numFaces(element);

        // reserve memory
        scvs_.resize(numScvs);
        scvfs_.reserve(numScvf);
        scvfIndicesOfScv_.resize(numScvs);
        localToGlobalScvfIndices_.resize(numScvs);
        hasBoundaryScvf_.resize(numScvs, false);

//         for (const auto& element : elements(this->gridView()))
//         {
//             auto eIdx = this->elementMapper().index(element);
//             std::cout << eIdx << " at " << element.geometry().center() << std::endl;
//         }

        const auto& velocityXPositions = intersectionMapper_->velocityXPositions();
        const auto& velocityYPositions = intersectionMapper_->velocityYPositions();

        // Build the scvs and scv faces
        IndexType scvfIdx = 0;
        numBoundaryScvf_ = 0;

        twoTimesCoarseFineInterfaceFaces_ = 0;
        twoTimesNonCoarseFineInterfaceFaces_ = 0;

        for (const auto& element : elements(this->gridView()))
        {
            auto eIdx = this->elementMapper().index(element);

            // reserve memory for the localToGlobalScvfIdx map
            auto numLocalFaces = intersectionMapper_->numFaces(element);
            localToGlobalScvfIndices_[eIdx].resize(numLocalFaces);

            scvs_[eIdx] = SubControlVolume(element.geometry(), eIdx);

            GeometryHelper geometryHelper(element, this->gridView(), intersectionMapper_, velocityXPositions, velocityYPositions);

            //At this point the intersection iteration should be my generic CVDIntersectionBase
            for (auto& intersection : intersectionMapper_->elementIntersections(element))
            {
                //TODO: Check if the global dof has been updated for element or not
                // (for the fine side where two elements have one cvdintersection)
                // also mark the element face which has the same global dof
                geometryHelper.updateLocalFace(intersection);
                const int localFaceIndex = geometryHelper.localFaceIndex();

                // Inner sub control volume faces
                if (intersection->neighbor())
                {
                    bool allowSCVFCreation = true;
                    //Check if the intersection has been handled already
                    if (!intersection->isSimple() && intersection->isInsideFiner())
                    {
                        if (markedFineToCoarseGlobalIndices_[intersection->globalIntersectionIndex()])
                        {
                            allowSCVFCreation = false;
                        }
                        else
                        {
                            markedFineToCoarseGlobalIndices_[intersection->globalIntersectionIndex()] = true;
                        }
                    }

                    //If hasn't been handled, create subcontrolvolumeface
                    if (allowSCVFCreation)
                    {
                        auto nIdx = this->elementMapper().index(intersection->outside());                        
                        if(intersection->isInsideFiner())
                        {
                            //TODO: Make a unique SCVF for each small grid cell (with correct scv index - currently not done like this)
                            auto e1Idx = this->elementMapper().index(intersection->intersections()[0].inside());
                            auto e2Idx = this->elementMapper().index(intersection->intersections()[1].inside());
                            scvfs_.emplace_back(intersection,
                                            scvfIdx,
                                            std::vector<IndexType>({e1Idx, e2Idx, nIdx}),
                                            geometryHelper);
                            localToGlobalScvfIndices_[e1Idx].resize(numLocalFaces);
                            localToGlobalScvfIndices_[e2Idx].resize(numLocalFaces);
                            localToGlobalScvfIndices_[e1Idx][localFaceIndex] = scvfIdx;
                            localToGlobalScvfIndices_[e2Idx][localFaceIndex] = scvfIdx;
                            scvfIndicesOfScv_[e1Idx].push_back(scvfIdx);
                            scvfIndicesOfScv_[e2Idx].push_back(scvfIdx++);
                        }
                        else
                        {
                            scvfs_.emplace_back(intersection,
                                            scvfIdx,
                                            std::vector<IndexType>({eIdx, nIdx}),
                                            geometryHelper);
                            localToGlobalScvfIndices_[eIdx][localFaceIndex] = scvfIdx;
                            scvfIndicesOfScv_[eIdx].push_back(scvfIdx++);
                        }
                    }

                    if (intersection->inside().level() != intersection->outside().level())
                    {
                        twoTimesCoarseFineInterfaceFaces_++;
                    }
                    else
                    {
                        twoTimesNonCoarseFineInterfaceFaces_++;
                    }
                }
                // Boundary sub control volume faces
                else if (intersection->boundary())
                {
                    scvfs_.emplace_back(intersection,
                                        scvfIdx,
                                        std::vector<IndexType>({eIdx, this->gridView().size(0) + numBoundaryScvf_++}),
                                        geometryHelper
                                        );
                    localToGlobalScvfIndices_[eIdx][localFaceIndex] = scvfIdx;
                    scvfIndicesOfScv_[eIdx].push_back(scvfIdx++);

                    hasBoundaryScvf_[eIdx] = true;

                    twoTimesNonCoarseFineInterfaceFaces_++;//those intersections visited only once, those increment by two
                    twoTimesNonCoarseFineInterfaceFaces_++;
                }
            }

        }

        coarseFineInterfaceFaces_ = twoTimesCoarseFineInterfaceFaces_/2.;
        nonCoarseFineInterfaceFaces_ = twoTimesNonCoarseFineInterfaceFaces_/2;

        std::cout << "interface = " << coarseFineInterfaceFaces_ << ", non interface " << nonCoarseFineInterfaceFaces_ << ", percentage = " << (double) coarseFineInterfaceFaces_/(coarseFineInterfaceFaces_+nonCoarseFineInterfaceFaces_) << std::endl;

        // fluxCorrectionGeometry_.update(*this);

        // build the connectivity map for an effecient assembly
        connectivityMap_.update(*this);
    }

    //! Get a sub control volume with a global scv index
    const SubControlVolume& scv(IndexType scvIdx) const
    {
        return scvs_[scvIdx];
    }

    //! Get a sub control volume face with a global scvf index
    const SubControlVolumeFace& scvf(IndexType scvfIdx) const
    {
        return scvfs_[scvfIdx];
    }

    //! Get the sub control volume face indices of an scv by global index
    const std::vector<IndexType>& scvfIndicesOfScv(IndexType scvIdx) const
    {
        return scvfIndicesOfScv_[scvIdx];
    }

    IndexType localToGlobalScvfIndex(IndexType eIdx, IndexType localScvfIdx) const
    {
        return localToGlobalScvfIndices_[eIdx][localScvfIdx];
    }

    const SubControlVolumeFace& scvf(IndexType eIdx ,IndexType localScvfIdx) const
    {
        return scvf(localToGlobalScvfIndex(eIdx, localScvfIdx));
    }

    /*!
     * \brief Returns the connectivity map of which dofs have derivatives with respect
     *        to a given dof.
     */
    const ConnectivityMap &connectivityMap() const
    { return connectivityMap_; }

    /*!
     * \brief Returns the flux correction geometry
     */
    const FluxCorrectionGeometry &fluxCorrectionGeometry() const
    { return fluxCorrectionGeometry_; }

    //! Returns a pointer the cell center specific auxiliary class. Required for the multi-domain FVAssembler's ctor.
    std::unique_ptr<MyCellCenterFVGridGeometry<ThisType>> cellCenterGridGeometryPtr() const
    {
        return std::make_unique<MyCellCenterFVGridGeometry<ThisType>>(this);
    }

    //! Returns a pointer the face specific auxiliary class. Required for the multi-domain FVAssembler's ctor.
    std::unique_ptr<MyFaceFVGridGeometry<ThisType>> faceGridGeometryPtr() const
    {
        return std::make_unique<MyFaceFVGridGeometry<ThisType>>(this);
    }

    //! Return a copy of the cell center specific auxiliary class.
    MyCellCenterFVGridGeometry<ThisType> cellCenterGridGeometry() const
    {
        return MyCellCenterFVGridGeometry<ThisType>(this);
    }

    //! Return a copy of the face specific auxiliary class.
    MyFaceFVGridGeometry<ThisType> faceGridGeometry() const
    {
        return MyFaceFVGridGeometry<ThisType>(this);
    }

    //! Returns whether one of the geometry's scvfs lies on a boundary
    bool hasBoundaryScvf(IndexType eIdx) const
    { return hasBoundaryScvf_[eIdx]; }

private:

    // mappers
    ConnectivityMap connectivityMap_;
    FluxCorrectionGeometry fluxCorrectionGeometry_;

    std::vector<SubControlVolume> scvs_;
    std::vector<SubControlVolumeFace> scvfs_;
    std::vector<std::vector<IndexType>> scvfIndicesOfScv_;
    std::vector<std::vector<IndexType>> localToGlobalScvfIndices_;
    std::unordered_map<int, bool> markedFineToCoarseGlobalIndices_;
    IndexType numBoundaryScvf_;
    std::vector<bool> hasBoundaryScvf_;

    std::size_t numIntersections_;

    unsigned int twoTimesCoarseFineInterfaceFaces_;
    unsigned int twoTimesNonCoarseFineInterfaceFaces_;

    unsigned int coarseFineInterfaceFaces_;
    unsigned int nonCoarseFineInterfaceFaces_;
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Base class for the finite volume geometry vector for staggered models
 *        This builds up the sub control volumes and sub control volume faces
 *        for each element. Specialization in case the FVElementGeometries are stored.
 */
template<class GV, class Traits>
class CVDStaggeredGridGeometry<GV, false, Traits>
{
    // TODO: implement without caching
};

} // end namespace

#endif
