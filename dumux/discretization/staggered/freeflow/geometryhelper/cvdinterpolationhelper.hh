// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDInterpolationHelper
 * \brief Class for methods related to interpolation procedures: finding of interpolation stencils and finding interpolation coeffs
 */
#ifndef DUMUX_COARSE_VEL_DISC_INTERPOLATION_HELPER_HH
#define DUMUX_COARSE_VEL_DISC_INTERPOLATION_HELPER_HH

#include <algorithm>
#include <type_traits>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelperutility.hh>
#include <dumux/common/cvdintersectionmapper.hh>


namespace Dumux {
template <class GridView>
class CVDInterpolationHelper {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

   private:
    const GridView gridView_;
    std::shared_ptr<CVDNonConformingGridIntersectionMapper<GridView>> intersectionMapper_;
    CVDGeometryHelperUtility<GridView> utility_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityXPositions_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityYPositions_;

   public:
    CVDInterpolationHelper(const GridView& gridView,
     std::shared_ptr<CVDNonConformingGridIntersectionMapper<GridView>> intersectionMapper,
     CVDGeometryHelperUtility<GridView>& utility)
        : gridView_(gridView),
        intersectionMapper_(intersectionMapper),
        utility_(utility),
        velocityXPositions_(intersectionMapper_->velocityXPositions()),
        velocityYPositions_(intersectionMapper_->velocityYPositions()) {}
    
    Scalar distance(const GlobalPosition& a, const GlobalPosition& b)
    {
        return std::sqrt((a[0]-b[0])*(a[0]-b[0]) + (a[1]-b[1])*(a[1]-b[1]));
    }

    /*!
    * \brief Finds the degrees of freedom and their locations in a parallel direction to the interface
    * @param isOne first intersection to which the other parallel intersection and their dofs will be found
    * @param allowShifting allows the shifting of the stencil in case of missing variables
    */
    std::vector<std::pair<GridIndexType, GlobalPosition>> threePointParallelInterpDofsAndLocations(CVDIntersectionBase<GridView, Intersection> *&is,
                                                                                                   GlobalPosition &pos)
    {
        std::vector<std::pair<GridIndexType, GlobalPosition>> stencil;

        //First central variable
        stencil.push_back({is->globalIntersectionIndex(), is->center()});

        //First closest neighbor variable
        std::vector<Intersection> intersections = is->intersections();
        Element e1 = intersections[0].inside();
        Element e2 = intersections[1].inside();        

        //Second closest neighbor variable
        std::vector<std::pair<GridIndexType, GlobalPosition>> parallelPairs1 = findParallelDofs(e1, e2, is);
        stencil.push_back(parallelPairs1[0]);
        //Third closest neighbor variable
        std::vector<std::pair<GridIndexType, GlobalPosition>> parallelPairs2 = findParallelDofs(e2, e1, is);
        stencil.push_back(parallelPairs2[0]);

        sort(stencil.begin(), stencil.end(), [&](const std::pair<GridIndexType, GlobalPosition> &a, const std::pair<GridIndexType, GlobalPosition> &b)
             { return (distance(a.second, pos) < distance(b.second, pos)); });
        return stencil;
    }

    /*!
    * \brief Finds the degrees of freedom and their locations in a parallel direction to the interface
    * @param isOne first intersection to which the other parallel intersection and their dofs will be found
    * @param isTwo second intersection to which the other parallel intersection and their dofs will be found
    * @param allowShifting allows the shifting of the stencil in case of missing variables
    */
    std::vector<std::pair<GridIndexType, GlobalPosition>> fourPointParallelInterpDofsAndLocations(CVDIntersectionBase<GridView, Intersection> *&isOne,
                                                                                                  CVDIntersectionBase<GridView, Intersection> *&isTwo,
                                                                                                  bool allowShifting)
    {
        std::vector<std::pair<GridIndexType, GlobalPosition>> stencil;
        Element e1 = isOne->inside();
        Element e2 = isTwo->inside();

        //First central variable
        stencil.push_back({isOne->globalIntersectionIndex(),isOne->center()});

        //Second central variable
        stencil.push_back({isTwo->globalIntersectionIndex(),isTwo->center()});
        
        //Parallel variables to the first central
        std::vector<std::pair<GridIndexType, GlobalPosition>> parallelPairs1 = findParallelDofs(e1, e2, isOne);
        
        //Parallel variables to the second central
        std::vector<std::pair<GridIndexType, GlobalPosition>> parallelPairs2 = findParallelDofs(e2, e1, isTwo);

        //Regular 4 - point symmetric stencil
        if(parallelPairs1.size()>0 && parallelPairs2.size()>0)
        {
            stencil.push_back(parallelPairs1[0]);
            stencil.push_back(parallelPairs2[0]);
        }
        //Shifted in direction towards isOne
        else if(allowShifting && parallelPairs1.size()>1)
        {
            stencil.insert(std::end(stencil), std::begin(parallelPairs1), std::end(parallelPairs1));
        }
        //Shifted in direction towards isTwo
        else if(allowShifting && parallelPairs2.size()>1)
        {
            stencil.insert(std::end(stencil), std::begin(parallelPairs2), std::end(parallelPairs2));
        }

        return stencil;
    }

    /*!
    *   \brief Finds a vector of (at most) two degrees of freedom and their locations in a parallel direction to the given @param is. If
    *   a parallel dof is missing or the further dof is missing, the function returns size 1 or 0 vector.
    *   @param ele normal intersections will be iterated over this element
    *   @param eleToIgnore normal intersection will be ignored if intersects with this element
    *   @param is intersection to which the parallel dofs will be searched
    */
    std::vector<std::pair<GridIndexType, GlobalPosition>> findParallelDofs(Element &ele,
                                                                      Element &eleToIgnore,
                                                                      CVDIntersectionBase<GridView, Intersection> *&is)
    {
        std::vector<std::pair<GridIndexType, GlobalPosition>> dofsAndLocations;

        for (auto normalIs : intersectionMapper_->elementIntersections(ele))
        {
            //Check if the normalIs.outside ele is not the one to ignore
            //Check if normalIs is normal to our reference intersection
            //Check if the intersections have a common corner
            if (normalIs->neighbor())
            {
                auto normalEle = normalIs->outside();
                if (utility_.index(normalEle) != utility_.index(eleToIgnore) &&
                    normalIs->isNormal(is) &&
                    haveCommonCornerTwoCVD(normalIs, is))
                {
                    //Check if the refinement level is the same for both elements
                    if (ele.level() == normalEle.level() || normalIs->isInsideFiner())
                    {
                        //It is the normalIs we need
                        //Now take normalIs.outside(), find the intersection with same normal as is

                        auto parallelIs = utility_.findSameDirectionIntersectionAsIs(is, normalEle, intersectionMapper_);
                        auto globalDof = parallelIs->globalIntersectionIndex();
                        
                        dofsAndLocations.push_back({globalDof, parallelIs->center()});

                        //Further dof
                        auto furtherNormalIs = utility_.findSameDirectionIntersectionAsIs(normalIs, normalEle, intersectionMapper_);
                        if (furtherNormalIs->neighbor())
                        {
                            auto furtherNormalEle = furtherNormalIs->outside();
                            if (ele.level() == normalIs->outside().level() && normalIs->neighbor())
                            {
                                auto furtherParallelIs = utility_.findSameDirectionIntersectionAsIs(is, furtherNormalEle, intersectionMapper_);
                                auto furtherGlobalDof = furtherParallelIs->globalIntersectionIndex();

                                dofsAndLocations.push_back({furtherGlobalDof, furtherNormalIs->center()});
                            }
                        }
                    }
                    //If outside of normalIs is coarser, we don't have the required variable
                    // else if(ele.level()>normalIs->outside().level()){}
                }
            }
        }

        return dofsAndLocations;
    }

    /*!
    * \brief Finds the degrees of freedom and their locations in a normal direction to the interface
    * @param isOne first intersection to which the other normal intersection and their dofs will be found
    * @param isTwo second intersection to which the other normal intersection and their dofs will be found
    * @param allowShifting allows the shifting of the stencil in case of missing variables
    */
    std::vector<std::pair<GridIndexType, GlobalPosition>> fourPointNormalInterpDofsAndLocations(CVDIntersectionBase<GridView, Intersection> *&isOne,
                                                                                                  CVDIntersectionBase<GridView, Intersection> *&isTwo,
                                                                                                  bool allowShifting)
    {
        std::vector<std::pair<GridIndexType, GlobalPosition>> stencil;

        //First central variable
        stencil.push_back({isOne->globalIntersectionIndex(),isOne->center()});

        //Second central variable
        stencil.push_back({isTwo->globalIntersectionIndex(),isTwo->center()});
        
        //Parallel variables to the first central
        std::vector<std::pair<GridIndexType, GlobalPosition>> normalPairs1 = findNormalDofs(isOne);
        
        //Parallel variables to the second central
        std::vector<std::pair<GridIndexType, GlobalPosition>> normalPairs2 = findNormalDofs(isTwo);

        //Regular 4 - point symmetric stencil
        if(normalPairs1.size()>0 && normalPairs2.size()>0)
        {
            stencil.push_back(normalPairs1[0]);
            stencil.push_back(normalPairs2[0]);
        }
        //Shifted in direction towards isOne
        else if(allowShifting && normalPairs1.size()>1)
        {
            stencil.insert(std::end(stencil), std::begin(normalPairs1), std::end(normalPairs1));
        }
        //Shifted in direction towards isTwo
        else if(allowShifting && normalPairs2.size()>1)
        {
            stencil.insert(std::end(stencil), std::begin(normalPairs2), std::end(normalPairs2));
        }

        return stencil;
    }

    std::vector<std::pair<GridIndexType, GlobalPosition>> findNormalDofs(CVDIntersectionBase<GridView, Intersection> *&is)
    {
        std::vector<std::pair<GridIndexType, GlobalPosition>> stencil;

        if(is->neighbor() && is->isSimple())
        {
            Element outerEle = is->outside();
            auto normalIs = utility_.findSameDirectionIntersectionAsIs(is, outerEle, intersectionMapper_);
            stencil.push_back({normalIs->globalIntersectionIndex(), normalIs->center()});

            if(normalIs->neighbor() && normalIs->isSimple())
            {
                Element furtherOuterEle = normalIs->outside();
                auto normalIs = utility_.findSameDirectionIntersectionAsIs(is, outerEle, intersectionMapper_);
                stencil.push_back({normalIs->globalIntersectionIndex(), normalIs->center()});
            }
        }

        return stencil;
    }

    void fourPointStencilInterpolation(std::vector<std::pair<GridIndexType, GlobalPosition>>& stencil,
                                       std::vector<int> &dofs,
                                       std::vector<Scalar> &interpFactors,
                                       const GlobalPosition &pos)
    {
        const bool linInterp = getParam<bool>("Adaptivity.LinearInterpolation", false);
        int dirIndex = findDirIndex(stencil);
        removeAllOffAxisDofs(stencil, dirIndex);
        if(linInterp || stencil.size()==2)
        {
            linearInterpolation(stencil, dofs, interpFactors, pos);
        }
        else
        {
            if(isStencilSymmetric(stencil, pos) && stencil.size() == 4)
            {
                for (auto pair : stencil)
                {
                    dofs.push_back(pair.first);
                }
                if (getParam<bool>("Adaptivity.UseOwn4PointCoefficients", true))
                {
                    interpFactors.push_back(0.5625);  //9/16
                    interpFactors.push_back(0.5625);  //9/16
                    interpFactors.push_back(-0.0625); //-1/16
                    interpFactors.push_back(-0.0625); //-1/16
                }
                else
                {
                    interpFactors.push_back(0.53125);  //1/2+1/32 vdP paper eq (4.53)
                    interpFactors.push_back(0.53125);  //1/2+1/32
                    interpFactors.push_back(-0.03125); //-1/32
                    interpFactors.push_back(-0.03125); //-1/32
                }
            }
            else
            {
                int nonDirIndex = findNonDirIndex(stencil);
                threePointStencilQuadraticInterpolation(stencil, dofs, interpFactors, nonDirIndex, pos);
            }
        }                    
    }

    void removeOffAxisDof(std::vector<std::pair<GridIndexType, GlobalPosition>>& stencil, int& dirIndex)
    {
        for (int i = 0; i < stencil.size(); i++)
        {
            if (!scalarCmp(stencil[i].second[dirIndex], stencil[0].second[dirIndex]))
            {
                stencil.erase(stencil.begin() + i);
                return;
            }
        }
    }

    void removeAllOffAxisDofs(std::vector<std::pair<GridIndexType, GlobalPosition>>& stencil, int& dirIndex)
    {
        auto stencilSize = stencil.size();
        removeOffAxisDof(stencil, dirIndex);
        bool removed = stencilSize != stencil.size();
        if(removed)
            removeAllOffAxisDofs(stencil, dirIndex);
        else
            return;
    }

    int findDirIndex(std::vector<std::pair<GridIndexType, GlobalPosition>>& stencil)
    {
        if(scalarCmp(stencil[0].second[0], stencil[1].second[0]))
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

    int findNonDirIndex(std::vector<std::pair<GridIndexType, GlobalPosition>>& stencil)
    {
        if(scalarCmp(stencil[0].second[0], stencil[1].second[0]))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }


    bool isStencilSymmetric(std::vector<std::pair<GridIndexType, GlobalPosition>> &stencil, const GlobalPosition &pos)
    {
        return scalarCmp(distance(pos, stencil[0].second), distance(pos, stencil[1].second)) 
            && scalarCmp(distance(pos, stencil[2].second), distance(pos, stencil[3].second));
    }

    void linearInterpolation(std::vector<std::pair<GridIndexType, GlobalPosition>>& stencil,
                                       std::vector<int> &dofs,
                                       std::vector<Scalar> &interpFactors,
                                       const GlobalPosition &pos)
    {
        Scalar totalDist = distance(stencil[0].second, stencil[1].second);
        for(int i=0;i<2;i++)
        {
            auto pair = stencil[i];
            dofs.push_back(pair.first);
            Scalar localDist = distance(pair.second, pos);
            interpFactors.push_back(1 - (localDist/totalDist));
        }
    }

    /*!
    * \brief Handles the 3 point symmetric linear interpolation.
    * @param stencil should be sorted by distance to pos
    * Hardcoded for now
    */
    void threePointStencilSymLinearInterpolation(std::vector<std::pair<GridIndexType, GlobalPosition>>& stencil,
                                       std::vector<int> &dofs,
                                       std::vector<Scalar> &interpFactors,
                                       const GlobalPosition &pos)
    {
        const bool linInterp = getParam<bool>("Adaptivity.LinearInterpolation", false);
        if (!linInterp)
        {
            for (auto pair : stencil)
            {
                dofs.push_back(pair.first);
            }
            interpFactors.push_back(1.);
            interpFactors.push_back(0.125);
            interpFactors.push_back(-0.125);
        }
        else
        {
            linearInterpolation(stencil, dofs, interpFactors, pos);
        }
    }

    /*!
    * \brief Handles the 3 point quadratic interpolation.
    * @param stencil should be sorted by distance to pos
    * Hardcoded for now
    */
    void threePointStencilQuadraticInterpolation(std::vector<std::pair<GridIndexType, GlobalPosition>>& stencil,
                                                 std::vector<int> &dofs,
                                                 std::vector<Scalar> &interpFactors,
                                                 const int& nonDirectionIndex,
                                                 const GlobalPosition &pos)
    {
        for (int p = 0; p < 3; p++)
        {
            auto pair = stencil[p];
            dofs.push_back(pair.first);
        }

        Dune::FieldMatrix<Scalar, 3, 3> threeCrossThreeMatrix;

        for (unsigned int i = 0; i < 3; ++i)
        {
            const Scalar delta = stencil[i].second[nonDirectionIndex] - pos[nonDirectionIndex];
            threeCrossThreeMatrix[i][0] = delta * delta;
            threeCrossThreeMatrix[i][1] = delta;
            threeCrossThreeMatrix[i][2] = 1;
        }

        threeCrossThreeMatrix.invert();

        interpFactors.push_back(threeCrossThreeMatrix[2][0]);
        interpFactors.push_back(threeCrossThreeMatrix[2][1]);
        interpFactors.push_back(threeCrossThreeMatrix[2][2]);
    }

void
replaceDofsHigherOrder_(std::vector<int> &dofs, std::vector<Scalar> &interpFactors, const GlobalPosition &pos, unsigned int directionIndex)
    {
        //note: if I want to come close to the boundary with this, I had a note that dof=-1 should be ignored and that in the case of oppo -1 can be accompanied by an actual dof
        if (dofs.size() > 1)
        {
            auto velocityPositions = (directionIndex==0)?velocityXPositions_:velocityYPositions_;
            bool isDof = (pairSecondFind(velocityPositions.begin(), velocityPositions.end(), pos) != velocityPositions.end());

            if (!isDof)
            {
                dofs.clear();
                interpFactors.clear();

                sort(velocityPositions.begin(), velocityPositions.end(), [&](const std::pair<GridIndexType, GlobalPosition> &a, const std::pair<GridIndexType, GlobalPosition> &b) {return (distance(a.second, pos) < distance(b.second, pos));});

                Dune::FieldMatrix<Scalar,6,6> sixCrossSixMatrix;
                Dune::FieldMatrix<Scalar,3,3> threeCrossThreeMatrix;

                int indexToBeReplaced = -1;
                unsigned int dir;

                if (checkFirstThreeAtSameXOrY_(velocityPositions, dir, pos))
                {
                    for (unsigned int i = 0; i<3; ++i)
                    {
                        dofs.push_back(velocityPositions[i].first);

                        const Scalar delta = velocityPositions[i].second[dir]-pos[dir];
                        threeCrossThreeMatrix[i][0] = delta*delta;
                        threeCrossThreeMatrix[i][1] = delta;
                        threeCrossThreeMatrix[i][2] = 1;
                    }

                    threeCrossThreeMatrix.invert();

                    interpFactors.push_back(threeCrossThreeMatrix[2][0]);
                    interpFactors.push_back(threeCrossThreeMatrix[2][1]);
                    interpFactors.push_back(threeCrossThreeMatrix[2][2]);
                }
                else
                {
                    unsigned int positiveDeltaXCount = 0;
                    unsigned int negativeDeltaXCount = 0;
                    unsigned int positiveDeltaYCount = 0;
                    unsigned int negativeDeltaYCount = 0;

                    Scalar eps = 1e-10;
                    unsigned int i = 0;
                    unsigned int matrixIndex = 0;
                    bool filledMatrix = false;

                    std::vector<unsigned int> pushedBackIndices;

                    while (filledMatrix == false)
                    {
                        if (checkIfMoreThanThreeAtSameXOrY_(velocityPositions, indexToBeReplaced, pos) && i == indexToBeReplaced)
                        {
                            ++i;
                            continue;
                        }

                        if (i > velocityPositions.size()-1)
                            DUNE_THROW(Dune::InvalidStateException, "i exceeds velocitypositions");

                        const Scalar deltaX = velocityPositions[i].second[0]-pos[0];
                        const Scalar deltaY = velocityPositions[i].second[1]-pos[1];

                        if (deltaX > eps)
                            ++positiveDeltaXCount;
                        else if (deltaX < -eps)
                            ++negativeDeltaXCount;
                        if (deltaY > eps)
                            ++positiveDeltaYCount;
                        else if (deltaY < -eps)
                            ++negativeDeltaYCount;

                        sixCrossSixMatrix[matrixIndex][0] = deltaX*deltaX;
                        sixCrossSixMatrix[matrixIndex][1] = deltaY*deltaY;
                        sixCrossSixMatrix[matrixIndex][2] = deltaX*deltaY;
                        sixCrossSixMatrix[matrixIndex][3] = deltaX;
                        sixCrossSixMatrix[matrixIndex][4] = deltaY;
                        sixCrossSixMatrix[matrixIndex][5] = 1;

                        if (matrixIndex == 3)
                        {
                            if (checkThatAtLeastNumXAndY_(velocityPositions, pushedBackIndices, indexToBeReplaced, matrixIndex, i))
                            {
                                pushedBackIndices.push_back(i);
                                dofs.push_back(velocityPositions[i].first);
                                ++matrixIndex;
                            }
                        }
                        else if (matrixIndex == 4)
                        {
                            if (checkThatAtLeastNumXAndY_(velocityPositions, pushedBackIndices, indexToBeReplaced, matrixIndex, i))
                            {
                                pushedBackIndices.push_back(i);
                                dofs.push_back(velocityPositions[i].first);
                                ++matrixIndex;
                            }
                        }
                        else if (matrixIndex >= 5)
                        {
                            if (((positiveDeltaXCount == 0 && negativeDeltaXCount == 0) || (positiveDeltaXCount > 0 && negativeDeltaXCount > 0)) &&
                                ((positiveDeltaYCount == 0 && negativeDeltaYCount == 0) || (positiveDeltaYCount > 0 && negativeDeltaYCount > 0)) &&
                                checkThatAtLeastNumXAndY_(velocityPositions, pushedBackIndices, indexToBeReplaced, matrixIndex, i)&&
                                checkNotThreePairs_(velocityPositions, pushedBackIndices, i))
                            {
                                pushedBackIndices.push_back(i);
                                dofs.push_back(velocityPositions[i].first);
                                filledMatrix=true;
                            }
                        }
                        else
                        {
                            pushedBackIndices.push_back(i);
                            dofs.push_back(velocityPositions[i].first);
                            ++matrixIndex;
                        }

                        ++i;
                    }

                    sixCrossSixMatrix.invert();

                    //last line contains interpolation factors
                    interpFactors.push_back(sixCrossSixMatrix[5][0]);
                    interpFactors.push_back(sixCrossSixMatrix[5][1]);
                    interpFactors.push_back(sixCrossSixMatrix[5][2]);
                    interpFactors.push_back(sixCrossSixMatrix[5][3]);
                    interpFactors.push_back(sixCrossSixMatrix[5][4]);
                    interpFactors.push_back(sixCrossSixMatrix[5][5]);
                }
            }
        }
    }

    bool checkFirstThreeAtSameXOrY_(const std::vector<std::pair<GridIndexType, GlobalPosition>>& velPos, unsigned int& dir, const GlobalPosition& pos)
    {
        for (unsigned int trialOtherDir = 0; trialOtherDir < 2; ++trialOtherDir)
        {
            dir = trialOtherDir==0?1:0;

            bool retVal = velPos[0].second[trialOtherDir] == pos[trialOtherDir] &&
                    velPos[1].second[trialOtherDir] == pos[trialOtherDir] &&
                    velPos[2].second[trialOtherDir] == pos[trialOtherDir];

            if (retVal == true)
                return true;
        }
        return false;
    }

    bool checkIfMoreThanThreeAtSameXOrY_(const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityPositions, int& indexToBeReplaced, const GlobalPosition& pos)
    {
        int trueCount = 0;
        for (unsigned int dir = 0; dir < 2; ++dir)//x,y
        {
            for (unsigned int i = 0; i < 6; ++i)
            {
                for (unsigned int j = i+1; j<6; ++j)
                {
                    if (velocityPositions[i].second[dir]==velocityPositions[j].second[dir] /*&& velocityPositions[i].second[dir]==pos[dir]*/)
                    {
                        trueCount++;

                        if (trueCount == 3)
                        {
                            indexToBeReplaced = j;
                        }
                    }
                }
            }
        }
        return (trueCount > 2);
    }

    //make sure we have three different x as well as three different y involved
    bool checkThatAtLeastNumXAndY_(const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityPositions,  const std::vector<unsigned int> pushedBackIndices, unsigned int indexToBeReplaced, unsigned int matrixIndex, unsigned int candidateIndex)
    {
        auto pushedBackIndicesVar = pushedBackIndices;
        pushedBackIndicesVar.push_back(candidateIndex);

        bool directionBool = true;

        for (unsigned int dir = 0; dir < 2; ++dir)//x,y
        {
            int differentValuesCountedUpToThree = 1;
            Scalar value1;
            Scalar value2;
            bool value2Set = false;

            value1 = velocityPositions[0].second[dir];

            for (const auto jIt : pushedBackIndicesVar)
            {
                if (velocityPositions[jIt].second[dir]==value1)
                {}
                else if (value2Set == true && velocityPositions[jIt].second[dir]==value2)
                {}
                else if (value2Set == false)
                {
                    differentValuesCountedUpToThree++;
                    value2 = velocityPositions[jIt].second[dir];
                    value2Set = true;
                }
                else
                {
                    differentValuesCountedUpToThree++;
                }
            }

            unsigned int num;
            if (matrixIndex == 5)
                num=3;
            else
                num=2;

            if (num==3 && differentValuesCountedUpToThree <= 2)
            {
                directionBool = false;
            }
            else if (num==2 && differentValuesCountedUpToThree <= 1)
            {
                directionBool = false;
            }
        }

        return directionBool;
    }

    //with this I forbid both three same x and three same y values
    bool checkNotThreePairs_(const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityPositions, const std::vector<unsigned int> pushedBackIndices, unsigned int candidateIndex)
    {
        auto pushedBackIndicesVar = pushedBackIndices;
        pushedBackIndicesVar.push_back(candidateIndex);

        bool directionBool = true;

        for (unsigned int dir = 0; dir < 2; ++dir)//x,y
        {
            int trueCount1 = 0;
            int trueCount2 = 0;
            int trueCount3 = 0;
            Scalar value1 = velocityPositions[0].second[dir];
            Scalar value2 = 0.;
            Scalar value3 = 0.;
            bool value2Set = false;
            bool value3Set = false;

            for (const auto& iIt : pushedBackIndicesVar)
            {
                for (const auto& jIt : pushedBackIndicesVar)
                {
                    if (velocityPositions[iIt].second[dir]==velocityPositions[jIt].second[dir])
                    {
                        if (velocityPositions[jIt].second[dir]==value1)
                        {
                            trueCount1++;
                        }
                        else if (value2Set && velocityPositions[jIt].second[dir]==value2)
                        {
                            trueCount2++;
                        }
                        else if (value3Set && velocityPositions[jIt].second[dir]==value3)
                        {
                            trueCount3++;
                        }
                    }
                    else if (value2Set == false)
                    {
                        value2 = velocityPositions[jIt].second[dir];
                        value2Set = true;
                    }
                    else if (value3Set == false)
                    {
                        if (velocityPositions[jIt].second[dir] !=value2 && velocityPositions[jIt].second[dir] != value1)
                        {
                            value3 = velocityPositions[jIt].second[dir];
                            value3Set = true;
                        }
                    }
                }
            }

            if (trueCount1==1 && trueCount2==1 && trueCount3==1)
            {
                directionBool = false;
            }
        }

        return directionBool;
    }

};
}  // namespace Dumux
#endif