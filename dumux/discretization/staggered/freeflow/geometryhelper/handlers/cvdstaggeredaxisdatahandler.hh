// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDStaggeredAxisDataHandler
 */
#ifndef DUMUX_COARSE_VEL_DISC_STAGGERED_AXIS_DATA_HANDLER_HH
#define DUMUX_COARSE_VEL_DISC_STAGGERED_AXIS_DATA_HANDLER_HH

#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelperutility.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdinterpolationhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdstaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelpergenericmethods.hh>
#include <dumux/discretization/staggered/intersection/cvdintersectionbase.hh>

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class for setting axis data
 */
template <class GridView, class IntersectionMapper, class AD, class GP>
class CVDStaggeredAxisDataHandler {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

   private:
    const GridView gridView_;
    std::shared_ptr<IntersectionMapper> intersectionMapper_;
    const Element element_;
    CVDIntersectionBase<GridView, Intersection>* intersection_;
    CVDGeometryHelperUtility<GridView> utility_;
    CVDInterpolationHelper<GridView> interpHelper_;
    AD axisData_;
    GP oppositePosition_;

    void fillOppositeDofs_()
    {
        CVDIntersectionBase<GridView, Intersection>* oppo = utility_.oppositeIntersection(element_, intersectionMapper_, intersection_);
        if(oppo->isSimple() || oppo->isOutsideFiner())
        {
            //Take DOF
            auto& oppoDofs = axisData_.oppositeDofs;
            auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

            oppoDofs.clear();
            oppoDofsInterp.clear();

            if(!oppo->boundary())
            {
                oppoDofs.push_back(oppo->globalIntersectionIndex());
                oppoDofsInterp.push_back(1.0);
            }
            else
            {
                oppoDofs.push_back(oppo->globalIntersectionIndex());
                // oppoDofs.push_back(-1);
                oppoDofsInterp.push_back(1.0);
                // oppoDofsInterp.push_back(0.5);
            }
        }
        else //if(oppo->isInsideFiner())
        {
            //Should handle the 3 - point interp.
            auto& oppoDofs = axisData_.oppositeDofs;
            auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

            oppoDofs.clear();
            oppoDofsInterp.clear();

            GlobalPosition pos = oppo->project(intersection_->center());

            // unsigned int directionIndex = oppo->directionIndex();
            std::vector<std::pair<GridIndexType, GlobalPosition>> stencil = interpHelper_.threePointParallelInterpDofsAndLocations(oppo, pos);
            interpHelper_.threePointStencilSymLinearInterpolation(stencil, oppoDofs, oppoDofsInterp, pos);         
        }
    }

    void fillOppositeDofsForTransition_(){

        if (intersection_->isInsideFiner())
        {
            //  HATCH "//" MARKS THE HALF-CONTROL VOLUME
            //   ___________________        ____________________
            //  |         |//  |    |       |    |  //|         |
            //  |         |//__|____|  OR   |____|__//|         |
            //  |         |//  |    |       |    |  //|         |
            //  |_________|//__|____|       |____|__//|_________|
            //

            //TODO: Modify localOppoIndices to be a map for two elements!

            std::vector<std::pair<Element, std::vector<int>>> localOppoIndices = utility_.localOppositeCVDIndices(intersection_);
            treatOppoFineToCoarse_(localOppoIndices);
        }
        else if (intersection_->isOutsideFiner())
        {
            //  HATCH "//" MARKS THE HALF-CONTROL VOLUME
            //   ___________________        ____________________
            //  |     ////|    |    |       |    |    |////     |
            //  |     ////|____|____|  OR   |____|____|////     |
            //  |     ////|    |    |       |    |    |////     |
            //  |_____////|____|____|       |____|____|////_____|
            //

            auto oppo = utility_.oppositeIntersection(element_, intersectionMapper_, intersection_);
            treatOppoCoarseToFine_(oppo);
        }
    }

    //  HATCH "//" MARKS THE HALF-CONTROL VOLUME
    //  van der Plas Figure 4.25 a (left)
    //  -----------------------------
    //  |             |      +      | parallel to first central
    //  |_____________|______|______|
    //  |             |///   |      |
    //  |             |///   +      | first central variable
    //  |             |///___|______|
    //  |             |///   |      | 
    //  |             |///   +      | second central variable
    //  |_____________|///___|______|
    //  |             |      |      |
    //  |             |      +      | parallel to second central
    //  -----------------------------
    // + : marks the variables to interpolate to get the velocity in the middle
    void treatOppoFineToCoarse_(const std::vector<std::pair<Element, std::vector<int>>>& localOppoIndicesMap)
    {
        auto& oppoDofs = axisData_.oppositeDofs;
        auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.clear();
        oppoDofsInterp.clear();                                

        //First central variable
        std::pair<Element, std::vector<int>> p1 = localOppoIndicesMap[0];
        Element e1 = p1.first;
        std::vector<int> oppo1 = p1.second;
        // oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(e1, oppo1[0]));
        CVDIntersectionBase<GridView, Intersection>* inter1 = intersectionMapper_->elementIntersection(e1, oppo1[0]);

        //Second central variable
        std::pair<Element, std::vector<int>> p2 = localOppoIndicesMap[1];
        Element e2 = p2.first;
        std::vector<int> oppo2 = p2.second;
        // oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(e2, oppo2[0]));        
        CVDIntersectionBase<GridView, Intersection>* inter2 = intersectionMapper_->elementIntersection(e2, oppo2[0]);

        GlobalPosition avgPosition = utility_.average_center(inter1->center(), inter2->center());        
        oppositePosition_ = avgPosition;

        std::vector<std::pair<GridIndexType, GlobalPosition>> dofLocations = interpHelper_.fourPointParallelInterpDofsAndLocations(inter1, inter2, true);
        
        interpHelper_.fourPointStencilInterpolation(dofLocations, oppoDofs, oppoDofsInterp, oppositePosition_);
    }

    

    void treatOppoCoarseToFine_(CVDIntersectionBase<GridView, Intersection> *& oppo)
    {
        auto& oppoDofs = axisData_.oppositeDofs;
        auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.clear();
        oppoDofsInterp.clear();
        
        //Only take single opposite global dof (coarse or refined will have same central position)
        oppositePosition_ = oppo->center();

        if(!oppo->boundary())
        {
        oppoDofs.push_back(oppo->globalIntersectionIndex());
        oppoDofsInterp.push_back(1.);
        }
        else
        {
            oppoDofs.push_back(oppo->globalIntersectionIndex());
            // oppoDofs.push_back(-1);
            oppoDofsInterp.push_back(1.0);
            // oppoDofsInterp.push_back(0.5);
        }
    }

   public:
    CVDStaggeredAxisDataHandler(const GridView& gridView,
     std::shared_ptr<IntersectionMapper> intersectionMapper,
      const Element& element,
      CVDIntersectionBase<GridView, Intersection>*& intersection,
      AD& axisData,
      GP& oppositePosition)
        : gridView_(gridView),
          intersectionMapper_(intersectionMapper),
          element_(element),
          intersection_(intersection),
          utility_(gridView, intersection),
          interpHelper_(gridView, intersectionMapper_, utility_),
          axisData_(axisData),
          oppositePosition_(oppositePosition) {}

    AD getAxisData(){
        return axisData_;
    }

    GP getOppositePosition(){
        return oppositePosition_;
    }

    /*!
     * \brief Fills all entries of the in axis data
     */
    void fillAxisData_() {
        // Clear the containers before filling them
        axisData_.inAxisForwardDofs.clear();
        axisData_.inAxisBackwardDofs.clear();
        axisData_.inAxisForwardDistances.clear();
        axisData_.inAxisBackwardDistances.clear();

        // Set the self Dof
        axisData_.selfDof = intersection_->globalIntersectionIndex();

        // Set the opposite Dof
        if (intersection_->isSimple())
        {
            fillOppositeDofs_();
        }
        else
        {
            fillOppositeDofsForTransition_();
        }
    }    

    //Method left undeleted only for reference
    void fillOppositeDofsOld_()
    {
        const auto localOppoIndices = utility_.localOppositeIndices_(intersectionMapper_->cvdisIndexInInside(intersection_), element_);

        if (!intersection_->neighbor() || (element_.level() >= intersection_->outside().level()))
        {
            //             !intersection_->neighbor()
            //           ------------/      ------------/
            //             |       ||/        |       ||/
            //             |  in   ||/      --|  in   ||/
            //             |       ||/        |       ||/
            //           ------------/      ------------/
            //
            //             /: boundary
            //             ||: intersection_

            //             inLevel > outLevel
            //             -----------------        -----------------
            //             |   |in||       |        |-|-|in||       |
            //             |--------  out  |        |--------  out  |
            //             |   |   |       |        |   |   |       |
            //             -----------------        -----------------
            //
            //             ||: intersection_

            //             inLevel = outLevel
            //           -------------------      -------------------
            //             |      ||       |        |      ||       |
            //             |  in  ||  out  |      --|  in  ||  out  |
            //             |      ||       |        |      ||       |
            //           -------------------      -------------------
            //
            //             ||: intersection_

            treatOppoInNotCoarserOut_(localOppoIndices);
        }
        else
        {
            if (localOppoIndices.size() == 1)
            {
                //            -->*
                //             -----------------
                //             |      ||out|   |
                //            --> in   |--------
                //             |       |   |   |
                //             -----------------
                //             ||: intersection_
                //             -->*: a velocity value from above is considered for the interpolation

                treatOppoInCoarserOutOneLocalOppo_(localOppoIndices);
            }
            else if (localOppoIndices.size() == 2)
            {
                //             -----------------
                //            -->     ||out|   |
                //             |  in   |--------
                //            -->      |   |   |
                //             -----------------
                //             ||: intersection_

                treatOppoInCoarserOutTwoLocalOppo_(localOppoIndices);
            }
            else
            {
                std::cout << "localOppositeIndices should be one or two. If there is a problem here please check if you are grading too extensively. E.g. if the smallest cell size is around 1e-10 this might not be a good idea." << std::endl;
            }
        }
    }

    void treatOppoInCoarserOutTwoLocalOppo_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = axisData_.oppositeDofs;
        auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.clear();
        oppoDofsInterp.clear();

        for (const auto& localOppoIndex : localOppoIndices)
        {
            const auto& oppoFacet = utility_.getFacet_(localOppoIndex, element_);

            //TODO adapt to 3d
            unsigned int nonDirectionIdx = (utility_.directionIndex() == 0) ? 1 : 0;

            if (scalarCmp(oppoFacet.geometry().center()[nonDirectionIdx], intersection_->center()[nonDirectionIdx]))
            {
                oppositePosition_ = oppoFacet.geometry().center();
                oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(element_, localOppoIndex));
                oppoDofsInterp.push_back(1.);
            }
        }
    }

    void treatOppoInNotCoarserOut_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = axisData_.oppositeDofs;
        auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.resize(localOppoIndices.size());
        for (unsigned int i = 0; i < localOppoIndices.size(); ++i)
        {
            const auto& oppoFacet = utility_.getFacet_(localOppoIndices[i], element_);
            oppoDofs[i] = intersectionMapper_->globalIntersectionIndex(element_, localOppoIndices[i]);
            oppositePosition_[utility_.directionIndex()] = oppoFacet.geometry().center()[utility_.directionIndex()];
            for (const auto nonDirectionIdx : utility_.nonDirectionIndices())
            {
                oppositePosition_[nonDirectionIdx] = intersection_->center()[nonDirectionIdx];
            }
        }

        if (oppoDofs.size() == 1)
        {
            oppoDofsInterp.resize(oppoDofs.size());
            oppoDofsInterp[0] = 1.0;
        }
        else if (oppoDofs.size() == 2)
        {
            oppoDofsInterp.resize(oppoDofs.size());
            oppoDofsInterp[0] = 0.5;
            oppoDofsInterp[1] = 0.5;
        }
        else
        {
            DUNE_THROW(Dune::InvalidStateException, "Expected oppoDofs.size to either be 1 or 2 but is not the case. If there is a problem here please check if you are grading too extensively. E.g. if the smallest cell size is around 1e-10 this might not be a good idea.");
        }
    }

    void treatOppoInCoarserOutOneLocalOppo_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = axisData_.oppositeDofs;
        auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.clear();
        oppoDofsInterp.clear();

        const auto& oppoFacet = utility_.getFacet_(localOppoIndices[0], element_);
        oppositePosition_[utility_.directionIndex()] = oppoFacet.geometry().center()[utility_.directionIndex()];
        for (const auto nonDirectionIdx : utility_.nonDirectionIndices())
        {
            oppositePosition_[nonDirectionIdx] = intersection_->center()[nonDirectionIdx];
        }

        oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(element_, localOppoIndices[0]));

        for (const auto& elementIs : intersections(gridView_, element_))
        {
            if (utility_.haveNonDirectionCornerCenterMatch_(elementIs) && utility_.facetIsNormal_(intersectionMapper_->cvdisIndexInInside(intersection_), intersectionMapper_->isIndexInInside(elementIs), element_))
            {
                if (elementIs.neighbor())
                {
                    auto minusNormal = intersection_->centerUnitOuterNormal();
                    minusNormal *= (-1);

                    if (elementIs.outside().level() == element_.level())
                    {
//             above here there can be a coarse cell with one velocity on the left, a coarse cell with two velocities on the left or four fine cells
//             in any of the cases, only the lower left velocity is considered
//             #       |
//             *********--------
//             |      ||out|   |
//             +      ||   |   |
//             |      ||   |   |
//            --> in   |--------
//             |       |   |   |
//             |       |   |   |
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: elementIs
//             #: neighborElemIntersection
//             +: interpolate a velocity here

                        for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                        {
                            if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal) && haveCommonNonDirectionalCornerCoordinate_(neighborElemIntersection, intersection_))
                            {
                                oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(elementIs.outside(), intersectionMapper_->isIndexInInside(neighborElemIntersection)));

                                Scalar distanceSelf = 0.5 * intersection_->volume(); //positive sign here is just a choice;//this is one quater of the opposite intersection, because the opposite intersection is twice the size of the self intersection in 2D
                                Scalar distanceClose = 0.5 * intersection_->volume() + 0.5 * neighborElemIntersection.geometry().volume(); //negative sign here is just a choice

                                Scalar interpFactSelf = distanceClose / (distanceSelf + distanceClose);
                                Scalar interpFactClose = distanceSelf / (distanceSelf + distanceClose);

                                oppoDofsInterp.push_back(interpFactSelf);
                                oppoDofsInterp.push_back(interpFactClose);
                            }
                        }
                    }
                    else if (elementIs.outside().level() > element_.level())
                    {
//             |   |   |
//             ****-****--------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: element intersections
                        if (!haveCommonCornerCVD(elementIs, intersection_) )
                        {
//             #   |   |
//             ****-------------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_oppoDofs
//             **: elementIs
//              #: neighbor element intersection
                            for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                            {
                                if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                {
                                    oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(elementIs.outside(), intersectionMapper_->isIndexInInside(neighborElemIntersection)));

                                    Scalar distanceSelf = 0.5 * intersection_->volume(); //positive sign here is just a choice;
                                    Scalar distanceClose = 0.5 * intersection_->volume() + 0.5 * neighborElemIntersection.geometry().volume(); //negative sign here is just a choice

                                    Scalar interpFactSelf = distanceClose / (distanceSelf + distanceClose);
                                    Scalar interpFactClose = distanceSelf / (distanceSelf + distanceClose);

                                    oppoDofsInterp.push_back(interpFactSelf);
                                    oppoDofsInterp.push_back(interpFactClose);
                                }
                            }
                        }
                    }
                    else
                    {
                        //          ________________________________________________________________________
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |_________________|_________________|                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n    element.outside                h
                        //         |                 |                 n                                   h
                        //         |_________________|_________________n==================_________________h
                        //         |                 |        |inter-  I                 |                 |
                        //         |                 |        |section.I                 |                 |
                        //         |                 |        |outside I                 |                 |
                        //         |                 |‾‾‾‾‾‾‾‾|‾‾‾‾‾‾‾‾|     element_    |                 |
                        //         |                 |        |        |                 |                 |
                        //         |_________________|________|________|_________________|_________________|
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |_________________|_________________|_________________|_________________|

                        //I: intersection_->volume()
                        //n: neighborElemIntersection.geometry().volume()
                        //=: elementIs

                        if(intersection_->neighbor())
                        {
                            //check if difference of elementIs.outside().level() and intersection_->outside().level() is >= 2
                            if(element_.level() < intersection_->outside().level())
                            {
                                //placerholder value for interpFactorSelf
                                oppoDofsInterp.push_back(0.0);

                                Scalar intersectionNormalCounter = 0.0; //count the number of oppoDofs/intersections with the same normal vector as intersection_
                                Scalar minusNormalCounter = 0.0;        //count intersections with opposite normal vector as intersection_
                                auto intersectionNormal = intersection_->centerUnitOuterNormal();
                                for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                                    { //in top neighboring elem we need dofs on all parallel intersections (parallel to intersection_) not just the one on the opposite
                                    //allow both directions for parallel normal vector of intersections (minus (opposite) and plus (in direction) of intersection_ normal
                                        if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal)
                                            || containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                        {
                                            if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                            {
                                                minusNormalCounter += 1.0; //count intersections with opposite normal vector as intersection_
                                            }

                                            if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                            {
                                                intersectionNormalCounter += 1.0; //count the number of oppoDofs/intersections with the same normal vector as intersection_
                                            }
                                        }
                                    }

                                    Scalar lengthForInterpFactorSelf = 0.0;

                                    for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                                    {
                                        Scalar interpFactor = 0.0;
                                        if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal)
                                            || containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                        {
                                            //oppoDofSelf was pushed back in the beginning of this whole function, need to assign a interpFactor to it
                                            if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                            {
                                                oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(elementIs.outside(), intersectionMapper_->isIndexInInside(neighborElemIntersection)));

                                                if(intersectionNormalCounter == 1) //should never occur, not permitted to levels difference for non-diagonals
                                                {
                                                    interpFactor = 0.5*(1.0 - (0.5*intersection_->volume() + 0.5*neighborElemIntersection.geometry().volume())/(intersection_->volume() + 0.5*neighborElemIntersection.geometry().volume()));
                                                }
                                                else if(intersectionNormalCounter == 2) //two intersections on the left side of coarse cell
                                                {
                                                    lengthForInterpFactorSelf = neighborElemIntersection.geometry().volume();

                                                    //interpFactor 0.5 from presequent interpolation already integrated into factor 0.25
                                                    interpFactor = 0.25*(1.0 - (0.5*intersection_->volume() + neighborElemIntersection.geometry().volume())/(intersection_->volume() + neighborElemIntersection.geometry().volume()));
                                                }

                                                oppoDofsInterp.push_back(interpFactor);
                                            }
                                            else if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                            {
                                                oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(elementIs.outside(), intersectionMapper_->isIndexInInside(neighborElemIntersection)));

                                                if(minusNormalCounter == 1) //one intersection on the right side of coarse cell
                                                {
                                                    interpFactor = 0.5*(1.0 - (0.5*intersection_->volume() + 0.5*neighborElemIntersection.geometry().volume())/(intersection_->volume() + 0.5*neighborElemIntersection.geometry().volume()));
                                                }
                                                else if(minusNormalCounter == 2) //two intersections on the right side of coarse cell
                                                {
                                                    //interpFactor 0.5 from presequent interpolation already integrated into factor 0.25
                                                    interpFactor = 0.25*(1.0 - (0.5*intersection_->volume() + neighborElemIntersection.geometry().volume())/(intersection_->volume() + neighborElemIntersection.geometry().volume()));
                                                }

                                                oppoDofsInterp.push_back(interpFactor);
                                            }
                                        }
                                    }

                                    Scalar interpFactorSelf = 0.0;
                                    interpFactorSelf = 1.0 - (0.5*intersection_->volume())/(intersection_->volume() + lengthForInterpFactorSelf);

                                    //insert right value for placeholder at [0] for self
                                    oppoDofsInterp[0] = interpFactorSelf;
                            }
                        }
                    }
                }
                else
                {
//             /////////
//             *********--------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: elementIs
//             //: boundary

                    //get Dirichlet boundary value and weight it by 0.5
                    oppoDofs.push_back(-1);
                    oppoDofsInterp.push_back(0.5);
                    oppoDofsInterp.push_back(0.5);
                }
            }
        }
    }


    bool haveCommonNonDirectionalCornerCoordinate_(const Intersection& isA, CVDIntersectionBase<GridView, Intersection>*& isB)
    {
        bool commonCoordinates = false;

        const auto nonDirIndices = utility_.nonDirectionIndices();

        if (nonDirIndices.size() != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Not yet prepared for 3D.");
        }

        for (const auto nonDirectionIdx : nonDirIndices)
        {
            std::vector<Scalar> isACorners;
            for (unsigned int i = 0; i < isA.geometry().corners(); ++i)
            {
                isACorners.push_back(isA.geometry().corner(i)[nonDirectionIdx]);
            }

            for (unsigned int i = 0; i < isB->corners(); ++i)
            {
                const auto isBCorner = isB->corner(i)[nonDirectionIdx];

                if (scalarFind_(isACorners.begin(), isACorners.end(), isBCorner) != isACorners.end())
                {
                    commonCoordinates = true;
                }
            }
        }

        return commonCoordinates;
    }
};

}  // namespace Dumux
#endif
