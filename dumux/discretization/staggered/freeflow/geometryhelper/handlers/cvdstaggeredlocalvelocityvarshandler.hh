// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDStaggeredLocalVelocityVarsHandler
 */
#ifndef DUMUX_COARSE_VEL_DISC_STAGGERED_LOCAL_FACE_VEL_HANDLER_HH
#define DUMUX_COARSE_VEL_DISC_STAGGERED_LOCAL_FACE_VEL_HANDLER_HH

#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelperutility.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdinterpolationhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdstaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelpergenericmethods.hh>
#include <dumux/discretization/staggered/intersection/cvdintersectionbase.hh>

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class for setting local velocity variables (for mass bal.)
 */
template <class GridView, class IntersectionMapper>
class CVDStaggeredLocalVelocityVarsHandler {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;   

   public:
    CVDStaggeredLocalVelocityVarsHandler(const GridView& gridView,
     std::shared_ptr<IntersectionMapper> intersectionMapper,
      const Element& element,
      CVDIntersectionBase<GridView, Intersection>*& intersection)
        : gridView_(gridView),
          element_(element),
          intersection_(intersection),
          utility_(gridView, intersection),
          interpHelper_(gridView, intersectionMapper, utility_) {}

    void fillVelVarsData(std::vector<MyLocalFaceVelocityData<Scalar>>& data)
    {
        data.clear();
        if(intersection_->isSimple() || intersection_->isOutsideFiner())
        {
            MyLocalFaceVelocityData<Scalar> simpleData{};
            simpleData.dofs.push_back(intersection_->globalIntersectionIndex());
            simpleData.interpolationFactors.push_back(1.0);
            data.push_back(simpleData);
        }
        else
        {
            for (auto is : intersection_->intersections())
            {
                MyLocalFaceVelocityData<Scalar> doubleData{};
                auto pos = is.geometry().center();
                auto stencil = interpHelper_.threePointParallelInterpDofsAndLocations(intersection_, pos);
                interpHelper_.threePointStencilSymLinearInterpolation(stencil, doubleData.dofs, doubleData.interpolationFactors, pos);
                data.push_back(doubleData);
            }
        }
    }

    private:
    const GridView gridView_;
    const Element element_;
    CVDIntersectionBase<GridView, Intersection>* intersection_;
    CVDGeometryHelperUtility<GridView> utility_;
    CVDInterpolationHelper<GridView> interpHelper_;
    
};

}  // namespace Dumux
#endif
