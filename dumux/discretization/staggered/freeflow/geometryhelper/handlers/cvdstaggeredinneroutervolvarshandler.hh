// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDStaggeredInnerOuterVolVarsHandler
 */
#ifndef DUMUX_COARSE_VEL_DISC_STAGGERED_VOLUME_VARS_HANDLER_HH
#define DUMUX_COARSE_VEL_DISC_STAGGERED_VOLUME_VARS_HANDLER_HH

#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdstaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelperutility.hh>

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class for setting inner and outer volume variables
 */
template <class GridView, class IntersectionMapper>
class CVDStaggeredInnerOuterVolVarsHandler {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

    private:
    const GridView gridView_;
    std::shared_ptr<IntersectionMapper> intersectionMapper_;
    const Element element_;
    CVDIntersectionBase<GridView, Intersection>* intersection_;
    CVDGeometryHelperUtility<GridView> utility_;

public:
    CVDStaggeredInnerOuterVolVarsHandler(const GridView &gridView,
                                         std::shared_ptr<IntersectionMapper> intersectionMapper,
                                         const Element &element,
                                         CVDIntersectionBase<GridView, Intersection> *&intersection)
        : gridView_(gridView),
          intersectionMapper_(intersectionMapper),
          element_(element),
          intersection_(intersection),
          utility_(gridView, intersection)
    {
    }
    /*!
     * \brief Fills all entries of the volume variables of the reference element
     */
    template <class ScvfBuildingData>
    void fillInnerOrOuterVolVarsData_(std::vector<int> &dofs, std::vector<Scalar> &interpFacts, std::vector<ScvfBuildingData> &volVarsScvfBuildingData, bool inOrOut /*0:in, 1:out*/)
    {
        dofs.clear();
        interpFacts.clear();
        volVarsScvfBuildingData.clear();

        if (inOrOut == 1 && !intersection_->neighbor())
        {
            dofs.push_back(-1);
            interpFacts.push_back(1.);

            ScvfBuildingData intersectionBuildingData;
            intersectionBuildingData.eIdx = gridView_.indexSet().index(element_);
            // intersectionBuildingData.localScvfIdx = intersectionMapper_->cvdisIndexInInside(intersection_);
            intersectionBuildingData.localScvfIdx = utility_.outerNormalIndex(intersection_);

            volVarsScvfBuildingData.push_back(intersectionBuildingData);

            return;
        }

        CVDIntersectionBase<GridView, Intersection> *is = (inOrOut == 0) ? intersection_ : outerIs_();

        // static const bool linearInterpolation = getParam<bool>("Adaptivity.LinearInterpolation", true);

        ScvfBuildingData emptyBuildingData;
        emptyBuildingData.eIdx = -1;
        emptyBuildingData.localScvfIdx = -1;

        //TODO figure out for 3D, get a distance for elementWidthSelf and so on, they are currently areas if the implementation is used for 3D
        //Interpolation explanation moved to bottom of this class

        //Seems like all cases for 1/2 interp. are solved by using inOrOut to choose the correct cvdintersection.
        if (is->neighbor() && is->isInsideFiner() && !is->isSimple())
        {            
            for(auto intersection:is->intersections())
            {
                dofs.push_back(gridView_.indexSet().index(intersection.inside()));
                interpFacts.push_back(0.5);
            }
            
            volVarsScvfBuildingData.push_back(emptyBuildingData);
        }
        else
        {
            dofs.push_back(gridView_.indexSet().index(is->inside()));
            interpFacts.push_back(1.);
            volVarsScvfBuildingData.push_back(emptyBuildingData);
        }
    }

private:
    //
    //          linear:                       quadratic:
    //          -------------                       ---------                 ---------              ---------
    //          |     |     |                       |   |   |                 |   |   |              |   |   |
    //          -------------                       ---------                 ---------              ---------
    //          |     |     |                       |   |   |                 |   |   |              |   |   |
    //          -------======-----------      ----------=====---------    --------=====--------     /----=====--------
    //          |     *    |*|         |      |     |   *  |*|       |    | |o|   *  |*|      |     /|   *  |*|      |
    //          |     o**x***     o    |      |  o  |   o*x**    o   |    -----   o*x**   o   |      o   o*x**   o   |
    //          |           |          |      |     |       |        |    | |o|       |       |     /|       |       |
    //          ------------------------      ------------------------    ---------------------     /-----------------
    //                                        <----><------><-------->
    //                                        width   width    width
    //                                        far     self     close
    //                or
    //          -------------                       ---------                ---------              ---------
    //          |     |     |                       |   |   |                |   |   |              |   |   |
    //          -------------                       ---------                ---------              ---------
    //          |     |     |                       |   |   |                |   |   |              |   |   |
    //          -------======-----------      ----------=====--------    --------=====--------     /----=====--------
    //          |     *    |*| o  |    |      |     |   *  |*|o |   |    | |o|   *  |*|o |   |     /|   *  |*|o |   |
    //          |     o**x***-----------      |  o  |   o*x**--------    -----   o*x**--------      O   o*x**--------
    //          |           |  o  |    |      |     |       | o |   |    | |o|       | o |   |     /|       | o |   |
    //          ------------------------      -----------------------    ---------------------     /-----------------
    //                                                                     <-><------><-->
    //                                                                  width  width  width
    //                                                                  far    self   close
    //                or
    //          -------------             ---------          ---------        ---------
    //          |     |     |             |   |   |          |   |   |        |   |   |
    //          -------------             ---------          ---------        ---------
    //          |     |     |             |   |   |          |   |   |        |   |   |
    //          -------====== /     ----------===== /    --------===== /     /----===== /
    //          |     *    |*|/     |     |   *  |*|/    | |o|   *  |*|/     /|   *  |*|/
    //          |     o**x**O /     |  o  |   o*x*O /    -----   o*x*O /      O   o*x*O /
    //          |           | /     |     |       | /    | |o|       | /     /|       | /
    //          ------------- /     ---------------      ------------- /     / -------- /
    //
    //  ***: half-control volume for the intersection
    //  1/4, 3/4: interpolation factors of those cell-centered quantities in the 2D equidistant case
    //  ===: intersection
    //  |*|: normal face
    //    o: dofs from which we interpolate
    //    x: virtual dof position
    //    O: In the case of a Neumann/Dirichlet boundary, there is no boundary value here. This position is not part of the dofs that we interpolate from. However, we still do factorInnter*innerValue+factorBoundary*boundaryValue while setting innerValue=boundaryValue. This is fine in the case of linear interpolation but in the case of quadratic interpolation //TODO solve this
    //       In the case of an outflow boundary, there is a boundary value here.

    void fillTransitionInnerOuterOld(CVDIntersectionBase<GridView, Intersection>*& is, std::vector<int> &dofs, std::vector<Scalar> &interpFacts, std::vector<ScvfBuildingData> &volVarsScvfBuildingData, ScvfBuildingData& emptyBuildingData, const bool& linearInterpolation)
    {
        // Let the quadratic function that is used for interpolation f(x)=a*x^2+b*x+c.
             // x=0 at the spot of interpolation,
             // distances have to carry the corrsponding signs, i.e. f(x=0)=c is the interpolated value
             // f_self = a*distanceSelf^2 + b*distanceSelf + c
             // f_close = a*distanceClose^2 + b*distanceClose + c
             // f_far = a*distanceFar^2 + b*distanceFar + c
             // solve the equation system for a,b,c
             // get c = prefactor_self(distanceSelf, distanceClose, distanceFar) * f_self +
             // prefactor_close(distanceSelf, distanceClose, distanceFar) * f_close +
             // prefactor_far(distanceSelf, distanceClose, distanceFar) * f_far

             bool isTwoLevelDiagonal = false;  //This is not treating all cases of two diagonal levels. There are other ones which just work fine in the !isTwoLevelDiagonal without special treatment.

             //check if diagonally differing by 2 levels
             for (const auto& elementIs : intersections(gridView_, is->inside())) {
                 if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_->cvdisIndexInInside(is), intersectionMapper_->isIndexInInside(elementIs), is->inside())) {
                     if (elementIs.neighbor()) {
                         if (is->outside().level() == elementIs.outside().level() + 2) {
                             isTwoLevelDiagonal = true;
                         }
                     }
                 }
             }

             if (isTwoLevelDiagonal) {
                 if (!linearInterpolation) {
                     DUNE_THROW(Dune::InvalidStateException, "No quadratic interpolation availabel for diagonally differing by two levels.");
                 }

                 //         ________________________________________________________________________
                 //         |                 |                 |                                   |
                 //         |                 |                 |                                   |
                 //         |                 |                 |                                   |
                 //         |                 |sameSizedElement |                                   |
                 //         |                 |                 |                                   |
                 //         |_________________|_________________|                                   |
                 //         |                 |                 h          largerElement            |
                 //         |                 |                 h                                   |
                 //         |                 |     closest     h                                   |
                 //         |                 |     element     h                                   |
                 //         |                 |                 h                                   |
                 //         |_________________|_________nnnnnnnn_wwwwwwwwwwwwwwwwww_________________|
                 //         |                 |        |smal=lers         =       |                 |
                 //         |                 |        |elem=ents         =       |                 |
                 //         |                 |________|____====s==========       |                 |
                 //         |                 |        |        |                 |                 |
                 //         |                 |        |        |     is->inside() |                 |
                 //         |_________________|________|________|_________________|_________________|
                 //         |                 |                 |                 |                 |
                 //         |                 |                 |                 |                 |
                 //         |                 |                 |                 |                 |
                 //         |                 |                 |                 |                 |
                 //         |                 |                 |                 |                 |
                 //         |_________________|_________________|_________________|_________________|

                 // n: borderToSmallerLength, Int_{Self} in report
                 // h: borderToLargerLength, Int_{Self,N} in report
                 // s: smallerLonelyLength, 2*Int_{neighb,N} in report
                 // w: largerLonelyLength, Int_{neighb} in report
                 // closestElement;/*Phi_2 in report*/
                 // smallerElement;/*Phi_1 in report*/
                 // sameSizedElement; /*Phi_3 in report*/
                 // largerElement;/*Phi_4, report*/
                 // =: control volume for the inspected DOF which lies in s; w and s replace = at times

                 Scalar borderToSmallerLength = is->volume();  //half of the height of elem_

                 Scalar borderToLargerLength = 0.;
                 Scalar largerLonelyLength = 0.;

                 for (const auto& elementIs : intersections(gridView_, is->inside())) {
                     if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_->cvdisIndexInInside(is), intersectionMapper_->isIndexInInside(elementIs), is->inside())) {
                         borderToLargerLength = elementIs.geometry().volume();  //width of is->inside()
                     }

                     if (elementIs.neighbor()) {
                         for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside())) {
                             if (haveCommonCornerCVD(neighborElemIntersection, is) && containerCmp(neighborElemIntersection.centerUnitOuterNormal(), is->centerUnitOuterNormal())) {
                                 largerLonelyLength = neighborElemIntersection.geometry().volume();
                             }
                         }
                     }
                 }

                 Scalar smallerLonelyLength = 0.;
                 for (const auto& intersectionInsideIs : intersections(gridView_, is->outside())) {
                     if (haveCommonCornerCVD(intersectionInsideIs, is) && facetIsNormal_(intersectionMapper_->cvdisIndexInInside(is), intersectionMapper_->isIndexInInside(intersectionInsideIs), is->inside())) {
                         smallerLonelyLength = intersectionInsideIs.geometry().volume();
                     }
                 }

                 Element closestElement = is->inside();
                 Element smallerElement = is->outside();

                 Element sameSizedElement = {};
                 Element largerElement = {};

                 //calculate interpFactors for other data points
                 for (const auto& elementIs : intersections(gridView_, is->inside())) {  //choose elem above self
                     if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_->cvdisIndexInInside(is), intersectionMapper_->isIndexInInside(elementIs), is->inside()))
                         largerElement = elementIs.outside();

                     auto minusNormal = is->centerUnitOuterNormal();
                     minusNormal *= (-1);

                     //choose elem right of self
                     if (containerCmp(elementIs.centerUnitOuterNormal(), minusNormal))  //cannot be refined on right side, dont need to check therefore
                         sameSizedElement = elementIs.outside();
                 }

                 utility_.fillIsTwoLevelDiagonal_(dofs, interpFacts, closestElement, smallerElement, sameSizedElement, largerElement, borderToLargerLength, borderToSmallerLength, smallerLonelyLength, largerLonelyLength, volVarsScvfBuildingData);
             } else {
                 //self
                 Scalar elementWidthSelf;
                 elementWidthSelf = 2. * is->volume();
                 Scalar distanceSelf = 0.25 * elementWidthSelf;  //positive sign here is just a choice

                 //close
                 std::vector<int> dofsClose;
                 Scalar elementWidthClose;
                 std::vector<ScvfBuildingData> buildingDataClose;

                 for (const auto& elementIs : intersections(gridView_, is->inside())) {
                     if (haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_->cvdisIndexInInside(is), intersectionMapper_->isIndexInInside(elementIs), is->inside())) {
                         fillCloseAndFar_(elementIs, elementWidthClose, dofsClose, buildingDataClose, is);
                     }
                 }
                 Scalar distanceClose = 0.25 * elementWidthSelf + 0.5 * elementWidthClose;  //negative sign here is just a choice

                 //far
                 std::vector<int> dofsFar;
                 Scalar elementWidthFar;
                 std::vector<ScvfBuildingData> buildingDataFar;
                 Scalar distanceFar = 0.;

                 if (!linearInterpolation) {
                     for (const auto& elementIs : intersections(gridView_, is->inside()))  //extra loop for far because of the break directive
                     {
                         if (!haveNonDirectionCornerCenterMatch_(elementIs) && facetIsNormal_(intersectionMapper_->cvdisIndexInInside(is), intersectionMapper_->isIndexInInside(elementIs), is->inside())) {
                             fillCloseAndFar_(elementIs, elementWidthFar, dofsFar, buildingDataFar, is);
                             break;
                         }
                     }
                     distanceFar = 0.75 * elementWidthSelf + 0.5 * elementWidthFar;
                 }

                 Scalar interpFactSelf;
                 if (linearInterpolation) {
                     interpFactSelf = distanceClose / (distanceSelf + distanceClose);
                 } else {
                     interpFactSelf = quadraticInterpolationFactor_(distanceSelf, distanceClose, distanceFar);
                 }

                 int dofSelf = gridView_.indexSet().index(is->inside());
                 dofs.push_back(dofSelf);
                 interpFacts.push_back(interpFactSelf);
                 volVarsScvfBuildingData.push_back(emptyBuildingData);

                 std::vector<Scalar> interpFactsClose;
                 for (unsigned int i = 0; i < dofsClose.size(); ++i) {
                     if (linearInterpolation) {
                         interpFactsClose.push_back(distanceSelf / ((distanceSelf + distanceClose) * dofsClose.size()));
                     } else {
                         interpFactsClose.push_back(quadraticInterpolationFactor_(distanceClose, distanceSelf, distanceFar) / dofsClose.size());
                     }
                 }
                 for (unsigned int i = 0; i < dofsClose.size(); ++i) {
                     dofs.push_back(dofsClose[i]);
                     interpFacts.push_back(interpFactsClose[i]);
                     volVarsScvfBuildingData.push_back(buildingDataClose[i]);
                 }

                 std::vector<Scalar> interpFactsFar;
                 if (!linearInterpolation) {
                     for (unsigned int i = 0; i < dofsFar.size(); ++i) {
                         interpFactsFar.push_back(quadraticInterpolationFactor_(distanceFar, distanceSelf, distanceClose) / dofsFar.size());
                     }
                     for (unsigned int i = 0; i < dofsFar.size(); ++i) {
                         dofs.push_back(dofsFar[i]);
                         interpFacts.push_back(interpFactsFar[i]);
                         volVarsScvfBuildingData.push_back(buildingDataFar[i]);
                     }
                 }
             }
    }

template<class ScvfBuildingData>
    void fillCloseAndFar_(const Intersection& elementIs, Scalar& elementWidth, std::vector<int>& dofs, std::vector<ScvfBuildingData>& buildingData, CVDIntersectionBase<GridView, Intersection>*& is)
    {
        if (!elementIs.neighbor())
        {
            ScvfBuildingData elementIsBuildingData;
            elementIsBuildingData.eIdx = gridView_.indexSet().index(elementIs.inside());
            elementIsBuildingData.localScvfIdx = intersectionMapper_->isIndexInInside(elementIs);

            buildingData.push_back(elementIsBuildingData);
            dofs.push_back(-1);
            elementWidth = 0.;
        }
        else
        {
            ScvfBuildingData emptyBuildingData;
            emptyBuildingData.eIdx = -1;
            emptyBuildingData.localScvfIdx = -1;

            dofs.push_back(gridView_.indexSet().index(elementIs.outside()));
            buildingData.push_back(emptyBuildingData);

            for (const auto& outsideElemIs : intersections(gridView_, elementIs.outside()))
            {
                if (containerCmp(outsideElemIs.centerUnitOuterNormal(), is->centerUnitOuterNormal()))
                {
                    if (!outsideElemIs.neighbor() ||
                        (elementIs.outside().level() == is->inside().level() && (outsideElemIs.inside().level() == outsideElemIs.outside().level()))
//          -------------
//          |  |  |     |
//          ------- out |
//          |  |  |     |
//          ---===-~~~~~|
//          |     :     |
//          |elem_: in  |
//          |     :     |
//          -------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs out:outside of outsideElemIs
                        ||
                        (elementIs.outside().level() > is->inside().level() && (outsideElemIs.inside().level() >= outsideElemIs.outside().level()))
//          -------------
//          |  |  |     |
//          ------- out |
//          |  |  |     |
//          ---===-~~---|
//          |     :in|  |
//          |elem_-------
//          |     |  |  |
//          -------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs out:outside of outsideElemIs
//          -------
//          |  |  |
//          ------- 3 different levels possible in this upper right corner (like elem_, like in, or even finer)
//          |  |  |
//          ---===-------
//          |     |ou|  |
//          |elem_-~~----
//          |     :in|  |
//          -------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs ou:outside of outsideElemIs
//          -------------
//          |  |  |  |  |
//          ------------|
//          |  |  |ou|  |
//          ---===-~~---|
//          |     :in|  |
//          |elem_-------
//          |     |  |  |
//          -------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs ou:outside of outsideElemIs
                    )
                    {
                        elementWidth = outsideElemIs.geometry().volume();
                    }
                    else if (elementIs.outside().level() == is->inside().level() && outsideElemIs.inside().level() < outsideElemIs.outside().level())
                    {
//          -------------
//          |  |  |  |  |
//          ----------- |
//          |  |  |ou|  |
//          ---===-~~---|
//          |     :     |
//          |elem_: in  |
//          |     :     |
//          -------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs ou:outside of outsideElemIs
                        elementWidth = 2 * outsideElemIs.geometry().volume();
                    }
                    else if (elementIs.outside().level() == is->inside().level() && outsideElemIs.inside().level() > outsideElemIs.outside().level())
                    {
//                --------------
//                |            |
//                |            |
//                |            |
//          -------    out     |
//          |  |  |            |
//          -------            |
//          |  |  |            |
//          ---===-~~~~~--------
//          |     :     |
//          |elem_: in  |
//          |     :     |
//          -------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs out:outside of outsideElemIs
                        DUNE_THROW(Dune::InvalidStateException, "two levels direct neighbors");
                    }
                    else if (elementIs.outside().level() < is->inside().level())
//          -------------
//          |  |  |     |
//          ------- out |
//          |  |  |     |
//          ---===-~~~~~--------
//          |     :            |
//          |elem_:            |
//          |     :            |
//          ------|     in     |
//                |            |
//                |            |
//                |            |
//                --------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs out:outside of outsideElemIs
                    {
                        DUNE_THROW(Dune::InvalidStateException, "two levels difference diagonal neighbors, should not enter here");
                    }
                    else if (elementIs.outside().level() > is->inside().level() && (outsideElemIs.inside().level() < outsideElemIs.outside().level()))
//          ---------------
//          |  |  | ? | ? |
//          --------------|
//          |  |  | | |   |
//          |  |  |---| ? |
//          |  |  | | |   |
//          ---===-~------|
//          |     :in |   |
//          |elem_--------|
//          |     |   |   |
//          ---------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs i:inside of outsideElemIs out:outside of outsideElemIs, ?: possibly also refined
                    {
                        // This is fine to be skipped, as the elementWidthClose will always be filled in the case of the following geometry which also will enter fillCloseAndFar_ before elementWidthClose is used.
//          -------
//          |  |  |
//          ------- 3 different levels possible in this upper right corner (like elem_, like in, or even finer)
//          |  |  |
//          ---===-------
//          |     |ou|  |
//          |elem_-~~----
//          |     :in|  |
//          -------------
//
//elem_: elment_    ===: is  :: elementIs  ~ : outsideElemIs in:inside of outsideElemIs ou:outside of outsideElemIs
                    }
                    else
                    {
                        DUNE_THROW(Dune::InvalidStateException, "should not happen");
                    }
                }
            }
        }
    }

    /*!
     * \brief Returns true if the intersection lies normal to another given intersection
     *
     * \param selfIdx The local index of the intersection itself
     * \param otherIdx The local index of the other intersection
     */
    bool facetIsNormal_(const int selfIdx, const int otherIdx, const Element& element) const
    {   
        return utility_.facetIsNormal_(selfIdx, otherIdx, element);
    }

    bool haveNonDirectionCornerCenterMatch_(const Intersection& isWithCenter)
    {
        return utility_.haveNonDirectionCornerCenterMatch_(isWithCenter);
    }

    bool haveNonDirectionCenterCornerMatch_(const Intersection& isWithCorners)
    {
        return utility_.haveNonDirectionCenterCornerMatch_(isWithCorners);
    }

    Scalar quadraticInterpolationFactor_(Scalar ownDistance, Scalar otherDistance1, Scalar otherDistance2)
    {
        return utility_.quadraticInterpolationFactor_(ownDistance, otherDistance1, otherDistance2);
    }

    CVDIntersectionBase<GridView, Intersection>* outerIs_()
    {
        for (const auto& inters : intersectionMapper_->elementIntersections(intersection_->outside()))
        {
            if (containerCmp(intersection_->center(), inters->center()))
            {
                return inters;
            }
        }
        DUNE_THROW(Dune::InvalidStateException, "Should not be outside of loop in outerIs_!");
    };
};

}  // namespace Dumux
#endif