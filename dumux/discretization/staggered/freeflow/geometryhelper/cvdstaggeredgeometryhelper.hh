// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDFreeFlowStaggeredGeometryHelper
 */
#ifndef DUMUX_COARSE_VEL_DISC_STAGGERED_GEOMETRY_HELPER_HH
#define DUMUX_COARSE_VEL_DISC_STAGGERED_GEOMETRY_HELPER_HH

#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/referenceelements.hh>

#include <dumux/common/math.hh>
#include <type_traits>
#include <algorithm>

#include <dumux/discretization/staggered/freeflow/staggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdstaggereddata.hh>

#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelpergenericmethods.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelperutility.hh>

#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/cvdstaggeredpairdatahandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/cvdstaggeredinneroutervolvarshandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/cvdstaggerednormalvolvarshandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/cvdstaggeredaxisdatahandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/cvdstaggeredlocalvelocityvarshandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdinterpolationhelper.hh>

#include <dumux/discretization/staggered/intersection/cvdintersectionbase.hh>
#include <dumux/discretization/staggered/intersection/cvdsimpleintersection.hh>
#include <dumux/discretization/staggered/intersection/cvddoubleintersection.hh>

namespace Dumux
{

/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class constructing the dual grid finite volume geometries
 *        for the free flow staggered discretization method.
 */
template<class GridView, class IntersectionMapper>
class CVDFreeFlowStaggeredGeometryHelper
{
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim-1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

    //TODO include assert that checks for quad geometry
    static constexpr int numPairs = 2 * (dimWorld - 1);
    static int order_;

public:
    using PairData = MyPairData<Scalar, GlobalPosition>;
    using AxisData = MyAxisData<Scalar>;

    CVDFreeFlowStaggeredGeometryHelper(const Element& element,
                                       const GridView& gridView,
                                       std::shared_ptr<IntersectionMapper> intersectionMapper,
                                       const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityXPositions,
                                       const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityYPositions)
        : element_(element),
          elementGeometry_(element.geometry()),
          gridView_(gridView),
          intersectionMapper_(intersectionMapper),
          velocityXPositions_(velocityXPositions),
          velocityYPositions_(velocityYPositions),
          utility_(gridView),
          interpUtility_(gridView, intersectionMapper_, utility_) {}

    //! update the local face
    void updateLocalFace(CVDIntersectionBase<GridView, Intersection>*& intersection)
    {
        intersection_ = intersection;
        utility_.setIntersection(intersection);

        //Fill pair data
        CVDStaggeredPairDataHandler pairDataHandler(gridView_, intersectionMapper_,
                                                    element_, intersection_, pairData_,
                                                    normalPairGlobalPositions_, parallelGlobalPositions_, this, axisData_);
        pairDataHandler.fillPairData_();
        pairData_ = pairDataHandler.getPairData();
        normalPairGlobalPositions_ = pairDataHandler.getNormalPairGlobalPositions();
        parallelGlobalPositions_ = pairDataHandler.getParallelGlobalPositions();
        axisData_ = pairDataHandler.getAxisData();


        //Fill axis data
        CVDStaggeredAxisDataHandler axisDataHandler(gridView_, intersectionMapper_, element_, intersection_, axisData_, oppositePosition_);
        axisDataHandler.fillAxisData_();
        axisData_ = axisDataHandler.getAxisData();
        oppositePosition_ = axisDataHandler.getOppositePosition();

        //Fill inner outer volume variables
        CVDStaggeredInnerOuterVolVarsHandler innerOuterVolVarsHandler(gridView_, intersectionMapper_, element_, intersection_);
        innerOuterVolVarsHandler.fillInnerOrOuterVolVarsData_(volVarsData_.innerVolVarsDofs, volVarsData_.innerVolVarsDofsInterpolationFactors, volVarsData_.innerVolVarsScvfBuildingData, 0/*in*/);
        innerOuterVolVarsHandler.fillInnerOrOuterVolVarsData_(volVarsData_.outerVolVarsDofs, volVarsData_.outerVolVarsDofsInterpolationFactors, volVarsData_.outerVolVarsScvfBuildingData, 1/*out*/);//empty if intersection_ at boundary

        //Fill normal volume variables
        CVDStaggeredNormalVolumeVarsHandler normalVolVarsHandler(gridView_, intersectionMapper_, element_, intersection_, volVarsData_);
        normalVolVarsHandler.fillNormalVolVarsDataNew_();
        // normalVolVarsHandler.fillNormalVolVarsData_();
        volVarsData_ = normalVolVarsHandler.getVolVarsData();

        //Fill element local face velocity variables (for mass bal. flux)
        CVDStaggeredLocalVelocityVarsHandler localVelVarsHandler(gridView_, intersectionMapper_, element_, intersection_);
        localVelVarsHandler.fillVelVarsData(localFaceVelocityData_);


        //Higher order interpolation remains from initial cloning
        static const bool higherOrderInterpolation = getParam<bool>("Adaptive.HigherOrderInterpolation", false);

        if (higherOrderInterpolation)
        {
            interpUtility_.replaceDofsHigherOrder_(axisData_.oppositeDofs, axisData_.oppositeDofsInterpolationFactors, oppositePosition_, directionIndex());

            for (unsigned int i = 0; i < numPairs; ++i)
            {
                interpUtility_.replaceDofsHigherOrder_(pairData_[i].parallelDofs[0],pairData_[i].parallelDofsInterpolationFactors[0],parallelGlobalPositions_[i], directionIndex());
                for (const auto nonDirectionIdx : nonDirectionIndices())
                {
                    interpUtility_.replaceDofsHigherOrder_(pairData_[i].normalPair.first,pairData_[i].normalPairInterpolationFactors.first,normalPairGlobalPositions_[i].first, nonDirectionIdx);
                    interpUtility_.replaceDofsHigherOrder_(pairData_[i].normalPair.second,pairData_[i].normalPairInterpolationFactors.second,normalPairGlobalPositions_[i].second, nonDirectionIdx);
                }
            }
        }
        int dofNo = getParam<int>("Adaptivity.DebugDof", -1);
        if(intersection_->globalIntersectionIndex() == dofNo)
        {
            // printDataForDebug();
            printDataForDebugConcise();
        }
    }

    /*!
     * \brief Returns the local index of the face (i.e. the intersection)
     */
    int localFaceIndex()
    {
        return utility_.outerNormalIndex(intersection_);
    }

    /*!
     * \brief Returns the local indices of the opposing faces
     */
    std::vector<int> localIndicesOpposingFace()
    {
        std::vector<int> returnVec;
        const auto onIdx = utility_.outerNormalIndex(intersection_);
        const auto dirIdx = intersection_->directionIndex();
        for(auto oppoIs : intersectionMapper_->elementIntersections(element_))
        {
            const auto oppoOnIdx = utility_.outerNormalIndex(oppoIs);
            const auto oppoDirIdx = oppoIs->directionIndex();
            if(dirIdx == oppoDirIdx && onIdx!=oppoOnIdx)
                returnVec.push_back(oppoOnIdx);
        }
        return returnVec;//localOppositeIndices_(inIdx, this->element_);
    }

    /*!
     * \brief Returns a copy of the axis data
     */
    auto axisData() const
    {
        return axisData_;
    }

    /*!
     * \brief Returns a copy of the pair data
     */
    auto pairData() const
    {
        return pairData_;
    }

    /*!
     * \brief Returns a copy of the volume variables data
     */
    auto volVarsData() const
    {
        return volVarsData_;
    }

    /*!
     * \brief Returns a copy of the local velocity variables data
     */
    auto localVelVarsData() const
    {
        return localFaceVelocityData_;
    }

    /*!
     * \brief Returns the dirction index of the primary facet (0 = x, 1 = y, 2 = z)
     */
    int directionIndex() const
    {
        return Dumux::directionIndex(std::move(intersection_->centerUnitOuterNormal()));
    }

    std::array<int, dim-1> nonDirectionIndices() const
    {
        return utility_.nonDirectionIndices();
    }

    /*!
     * \brief Returns the dirction index of the facet passed as an argument (0 = x, 1 = y, 2 = z)
     */
    int directionIndex(const Intersection& intersection) const
    {
        return utility_.directionIndex(intersection);
    }

    //! \brief Returns the order needed by the scheme
    static int order()
    {
        return order_;
    }

    //! \brief Set the order needed by the scheme
    static void setOrder(const int order)
    {
        if (order != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Adaptive only works for first order.");
        }
        order_ = order;
    }

private:
void printDataForDebugConcise()
{
    std::ostream& s = std::cout;
    s << "==== GEOMETRY HELPER ==== GLOBAL DOF IDX:"<< intersection_->globalIntersectionIndex();
    s << std::endl << "element center = " << element_.geometry().center() 
        << ", intersection center = " << intersection_->center() << std::endl;
    s << "Intersection is simple: "<<intersection_->isSimple() << std::endl;
    s << "Normal: " << intersection_->centerUnitOuterNormal() << std::endl;
    s << std::endl;
    s << "   = BASE INFO =   "<< std::endl;
    s << " scvf area: "<< intersection_->volume() << std::endl;
    s << " opposite dist: "<< axisData_.selfToOppositeDistance << std::endl;
    s << " parallel dist 1: "<< pairData_[0].parallelDistances[1] << std::endl;
    s << " parallel dist 2: "<< pairData_[1].parallelDistances[1] << std::endl;
    s << " normal area 1: "<< "not known at this point" << std::endl;
    s << " normal area 2: "<< "not known at this point" << std::endl;
    s << " self dof: "<< axisData_.selfDof << std::endl;    

    s << "   = OPPO VELOCITY =   " << std::endl;
        for (const auto &oppositeDof : axisData_.oppositeDofs)
        {
            s << oppositeDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : axisData_.oppositeDofsInterpolationFactors)
        {
            s << factor << ", ";
        }
        s << std::endl;
    s << "   = PARALLEL VELOCITY 1 =   " << std::endl;
        for (const auto &parallelDof : pairData_[0].parallelDofs[0])
        {
            s << parallelDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : pairData_[0].parallelDofsInterpolationFactors[0])
        {
            s << factor << ", ";
        }
        s << std::endl;
    s << "   = PARALLEL VELOCITY 2 =   " << std::endl;
        for (const auto &parallelDof : pairData_[1].parallelDofs[0])
        {
            s << parallelDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : pairData_[1].parallelDofsInterpolationFactors[0])
        {
            s << factor << ", ";
        }
        s << std::endl;
    s << "   = NORMAL INSIDE VELOCITY 1 =   " << std::endl;
        for (const auto &normalFirstDof : pairData_[0].normalPair.first)
        {
            s << normalFirstDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : pairData_[0].normalPairInterpolationFactors.first)
        {
            s << factor << ", ";
        }
        s << std::endl;
    s << "   = NORMAL INSIDE VELOCITY 2 =   " << std::endl;
        for (const auto &normalFirstDof : pairData_[1].normalPair.first)
        {
            s << normalFirstDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : pairData_[1].normalPairInterpolationFactors.first)
        {
            s << factor << ", ";
        }
        s << std::endl;
    s << "   = NORMAL OUTSIDE VELOCITY 1 =   " << std::endl;
        for (const auto &normalSecondDof : pairData_[0].normalPair.second)
        {
            s << normalSecondDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : pairData_[0].normalPairInterpolationFactors.second)
        {
            s << factor << ", ";
        }
        s << std::endl;
    s << "   = NORMAL OUTSIDE VELOCITY 2 =   " << std::endl;
        for (const auto &normalSecondDof : pairData_[1].normalPair.second)
        {
            s << normalSecondDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : pairData_[1].normalPairInterpolationFactors.second)
        {
            s << factor << ", ";
        }
        s << std::endl;
    s << "   = INNER VOL VAR =   : "<< std::endl;
        for (const auto &innerVolVarDof : volVarsData_.innerVolVarsDofs)
        {
            s << innerVolVarDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : volVarsData_.innerVolVarsDofsInterpolationFactors)
        {
            s << factor << ",";
        }
        s << std::endl;
    s << "   = OUTER VOL VAR =   : "<< std::endl;
        for (const auto &outerVolVarDof : volVarsData_.outerVolVarsDofs)
        {
            s << outerVolVarDof << ", ";
        }
        s << std::endl;
        for (const auto &factor : volVarsData_.outerVolVarsDofsInterpolationFactors)
        {
            s << factor << ",";
        }
        s << std::endl;

}

 void printDataForDebug() {
     std::ostream& s = std::cout;
     s<<std::defaultfloat;
     s << "==== GEOMETRY HELPER ==== GLOBAL DOF IDX:"<< intersection_->globalIntersectionIndex();
     s << std::endl
               << "element center = " << element_.geometry().center() << ", intersection center = " << intersection_->center() << std::endl;
     bool listBaseInfo = true;
     bool checkAxisData = true;
     bool checkFirstNormalPairData = true;
     bool checkSecondNormalPairData = true;
     bool checkParallelPairData = true;
     bool checkInnerVolVarDofs = true;
     bool checkOuterVolVarDofs = true;
     bool checkNormalVolVarDofs = true;

     if (listBaseInfo)
     {
         for (unsigned int i = 0; i < numPairs; ++i)
         {
             s << "pairData of pair " << i << ":" << std::endl;

             s << "parallel dofs = ";
             for (const auto &prallelDof : pairData_[i].parallelDofs[0 /*order*/])
             {
                 s << prallelDof << ", ";
             }
             s << std::endl;
             s << "parallelGlobalPositions_ = " << parallelGlobalPositions_[i] << std::endl;

             s << "parallelSelfDistance = " << pairData_[i].parallelDistances[0] << std::endl;
             s << "parallelNextDistance = " << pairData_[i].parallelDistances[1] << std::endl;

             s << "normalfirst = ";
             for (const auto &normalFirstDof : pairData_[i].normalPair.first)
             {
                 s << normalFirstDof << ", ";
             }
             s << std::endl;
             s << "first normal pos = " << normalPairGlobalPositions_[i].first << std::endl;

             s << "normalsecond = ";
             for (const auto &normalSecondDof : pairData_[i].normalPair.second)
             {
                 s << normalSecondDof << ", ";
             }
             s << std::endl;
             s << "second normal pos = " << normalPairGlobalPositions_[i].second << std::endl;

             s << "localNormalFluxCorrectionIndex = " << pairData_[i].localNormalFluxCorrectionIndex << std::endl;

             s << "normalDistance = " << pairData_[i].normalDistance << std::endl;

             s << "virtualFirstParallelFaceDofPos = " << pairData_[i].virtualFirstParallelFaceDofPos << std::endl;
         }
     }
    Scalar sum = 0.;
     if (checkAxisData)
     {
         s << "axis data:" << std::endl;

         s << "self dof = " << axisData_.selfDof << std::endl;

         s << "opposite dofs = ";
         for (const auto &oppositeDof : axisData_.oppositeDofs)
         {
             s << oppositeDof << ", ";
         }
         s << std::endl;
         s << "oppositePosition_= " << oppositePosition_ << std::endl;

         s << "inAxisForwardDofs = ";
         for (const auto &inAxisForwardDof : axisData_.inAxisForwardDofs)
         {
             s << inAxisForwardDof << ", ";
         }
         s << std::endl;

         s << "inAxisBackwardDofs = ";
         for (const auto &inAxisBackwardDof : axisData_.inAxisBackwardDofs)
         {
             s << inAxisBackwardDof << ", ";
         }
         s << std::endl;

         s << "selfToOppositeDistance = " << axisData_.selfToOppositeDistance << std::endl;

         s << "inAxisForwardDistances = ";
         for (const auto &inAxisForwardDistance : axisData_.inAxisForwardDistances)
         {
             s << inAxisForwardDistance << ", ";
         }
         s << std::endl;

         s << "inAxisBackwardDistances = ";
         for (const auto &inAxisBackwardDistance : axisData_.inAxisBackwardDistances)
         {
             s << inAxisBackwardDistance << ", ";
         }
         s << std::endl;

         //check sums of interpolation factors
         s << "oppoFactors ";
         for (const auto &factor : axisData_.oppositeDofsInterpolationFactors)
         {
             s << factor << ", ";
             sum += factor;
         }
         s << std::endl;
         if (!scalarCmp(sum, 1.))
         {
             DUNE_THROW(Dune::InvalidStateException, "Opposite dofs interpolation factors have to sum up to one, but they sum up to" << sum << ".");
         }
     }

     if (checkFirstNormalPairData)
     {
         for (const auto &data : pairData_)
         {
             sum = 0.;
             s << "normalPairFirstFactors ";
             for (const auto &factor : data.normalPairInterpolationFactors.first)
             {
                 s << factor << ", ";
                 sum += factor;
             }
             s << std::endl;
             if (!scalarCmp(sum, 1.))
             {
                 DUNE_THROW(Dune::InvalidStateException, "Normal pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
             }
         }
     }

     if (checkSecondNormalPairData)
     {
         if (intersection_->neighbor())
         {
             for (const auto &data : pairData_)
             {
                 sum = 0.;
                 s << "normalPairsecondFactors ";
                 for (const auto &factor : data.normalPairInterpolationFactors.second)
                 {
                     s << factor << ", ";
                     sum += factor;
                 }
                 s << std::endl;
                 if (!scalarCmp(sum, 1.))
                 {
                     DUNE_THROW(Dune::InvalidStateException, "Normal pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
                 }
             }
         }
     }

     if (checkParallelPairData)
     {
         for (const auto &data : pairData_)
         {
             for (unsigned int i = 0; i < data.parallelDofsInterpolationFactors.size(); ++i)
             {
                 sum = 0.;

                 s << "parallelFactors ";
                 for (const auto &factor : data.parallelDofsInterpolationFactors[i])
                 {
                     s << factor << ", ";
                     sum += factor;
                 }
                 s << std::endl;

                 if (!scalarCmp(sum, 1.0) && data.parallelDofs[i][0] != -1)
                 {
                     DUNE_THROW(Dune::InvalidStateException, "Parallel pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
                 }
             }
         }
     }

     if (checkInnerVolVarDofs)
     {
         s << "innerVolVarsDofs = ";
         for (const auto &innerVolVarDof : volVarsData_.innerVolVarsDofs)
         {
             s << innerVolVarDof << ", ";
         }
         s << std::endl;

         s << "innerVolVarsDofsInterpolationFactors = ";
         sum = 0.;
         for (const auto &factor : volVarsData_.innerVolVarsDofsInterpolationFactors)
         {
             s << factor << ",";
             sum += factor;
         }
         s << std::endl;

         if (!scalarCmp(sum, 1.0))
         {
             DUNE_THROW(Dune::InvalidStateException, "vol vars inner interpolation factors have to sum up to one, but they sum up to" << sum << ".");
         }
     }

     if (checkOuterVolVarDofs)
     {
         s << "outerVolVarsDofs = ";
         for (const auto &outerVolVarDof : volVarsData_.outerVolVarsDofs)
         {
             s << outerVolVarDof << ", ";
         }
         s << std::endl;

         s << "outerVolVarsDofsInterpolationFactors = ";
         sum = 0.;
         for (const auto &factor : volVarsData_.outerVolVarsDofsInterpolationFactors)
         {
             s << factor << ",";
             sum += factor;
         }
         s << std::endl;

         if (!scalarCmp(sum, 1.0))
         {
             DUNE_THROW(Dune::InvalidStateException, "vol vars outer interpolation factors have to sum up to one, but they sum up to" << sum << "."); //empty if intersection at boundary
         }
     }

     if (checkNormalVolVarDofs)
     {
         s << "normalVolVarsDofs = ";
         for (const auto &normalVolVarDofs : volVarsData_.normalVolVarsDofs)
         {
             s << std::endl;
             for (const auto &normalVolVarDof : normalVolVarDofs)
                 s << normalVolVarDof << ", ";
         }
         s << std::endl;

         s << "normalVolVarsDofsInterpolationFactors = ";
         for (const auto &factors : volVarsData_.normalVolVarsDofsInterpolationFactors)
         {
             s << std::endl;
             sum = 0.;
             for (const auto &factor : factors)
             {
                 s << factor << ",";
                 sum += factor;
             }
             s << ", sum is " << sum << std::endl;
         }
         s << std::endl;

         if (!scalarCmp(sum, 1.0))
         {
             DUNE_THROW(Dune::InvalidStateException, "vol vars normal interpolation factors have to sum up to one, but they sum up to" << sum << ".");
         }
     }
 }

    Scalar quadraticInterpolationFactor_(Scalar ownDistance, Scalar otherDistance1, Scalar otherDistance2)
    {
        return otherDistance1 * otherDistance2 / ((otherDistance1 - ownDistance) * (otherDistance2 - ownDistance));
    }

    /*!
     * \brief Returns the local opposing intersection index
     *
     * \param idx The local index of the intersection itself
     */
    std::vector<int> localOppositeIndices_(const int idx, const Element& element) const
    {
        return utility_.localOppositeIndices_(idx, element);
    }

    //returns the index of self and of parallelSelf
    std::vector<int> localSelfIndices_(const int idx, const Element& element) const
    {
        return utility_.localSelfIndices_(idx, element);
    }

    /*!
     * \brief Returns true if the intersection lies normal to another given intersection
     *
     * \param selfIdx The local index of the intersection itself
     * \param otherIdx The local index of the other intersection
     */
    bool facetIsNormal_(const int selfIdx, const int otherIdx, const Element& element) const
    {
        return utility_.facetIsNormal_(selfIdx, otherIdx, element);
    };

    Intersection getFacet_(const int localFacetIdx, const Element& element) const
    {
        return utility_.getFacet_(localFacetIdx, element);
    };

    CVDIntersectionBase<GridView, Intersection>* intersection_; //!< The intersection of interest
    const Element element_; //!< The respective element
    const typename Element::Geometry elementGeometry_; //!< Reference to the element geometry
    const GridView gridView_; //!< The grid view
    std::shared_ptr<IntersectionMapper> intersectionMapper_;
    MyAxisData<Scalar> axisData_; //!< Data related to forward and backward faces
    std::array<MyPairData<Scalar, GlobalPosition>, numPairs> pairData_; //!< Collection of pair information related to normal and parallel faces
    MyVolVarsData<Scalar, numPairs> volVarsData_;
    std::vector<MyLocalFaceVelocityData<Scalar>> localFaceVelocityData_;
    std::array<std::pair<GlobalPosition,GlobalPosition>, numPairs> normalPairGlobalPositions_;
    std::array<GlobalPosition, numPairs> parallelGlobalPositions_;
    GlobalPosition oppositePosition_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityXPositions_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityYPositions_;
    CVDGeometryHelperUtility<GridView> utility_;
    CVDInterpolationHelper<GridView> interpUtility_;
};

template<class GridView, class IntersectionMapper>
int CVDFreeFlowStaggeredGeometryHelper<GridView, IntersectionMapper>::order_ = 1;

} // end namespace Dumux

#endif
