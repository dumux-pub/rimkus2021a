// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDGeometryHelperUtility
 * \brief Class for methods used throughout geometryhelper - related classes
 */
#ifndef DUMUX_COARSE_VEL_DISC_GH_UTILITY_HH
#define DUMUX_COARSE_VEL_DISC_GH_UTILITY_HH

#include <algorithm>
#include <type_traits>

#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelpergenericmethods.hh>

#include <dumux/discretization/staggered/intersection/cvdintersectionbase.hh>

namespace Dumux {
template <class GridView>
class CVDGeometryHelperUtility {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

   private:
    const GridView gridView_;
    CVDIntersectionBase<GridView, Intersection>* intersection_;

   public:
    CVDGeometryHelperUtility(const GridView& gridView)
        : gridView_(gridView) {}

    CVDGeometryHelperUtility(const GridView& gridView, CVDIntersectionBase<GridView, Intersection>*& intersection)
        : gridView_(gridView),
          intersection_(intersection) {}

    void setIntersection(CVDIntersectionBase<GridView, Intersection>*& intersection){
        intersection_ = intersection;
    }

    GridIndexType index(const Element& ele){
        return gridView_.indexSet().index(ele);
    }

    std::map<Element, std::vector<int>> localSelfCVDIndices(CVDIntersectionBase<GridView, Intersection>*& intersection) const
    {
        std::map<Element, std::vector<int>> indexMap;
        for(auto is:intersection->intersections()){
            const auto ele = is.inside();
            std::vector<int> localSelfIndices = localSelfIndicesDirect_(is, ele);
            indexMap[ele] = localSelfIndices;
        }
        return indexMap;
    }

    template<class IntersectionMapper>
    CVDIntersectionBase<GridView, Intersection>* oppositeIntersection(const Element& ele,
     IntersectionMapper& intersectionMapper,
     CVDIntersectionBase<GridView, Intersection>*& intersection)
    {
        for(auto is:intersectionMapper->elementIntersections(ele))
        {
            if(isNormalOpposite(is, intersection))
                return is;
        }
        DUNE_THROW(Dune::InvalidStateException, "Could not find opposite intersection!");
        return intersection;
    }

    /*!
    * \brief Returns oppositeIndices for each element of the cvdIntersection
    * \param intersection cvdIntersection for the search of oppositeIndices
    */
    std::vector<std::pair<Element, std::vector<int>>> localOppositeCVDIndices(CVDIntersectionBase<GridView, Intersection>*& intersection) const
    {
        std::vector<std::pair<Element, std::vector<int>>> eles;
        for(const Intersection is:intersection->intersections()){
            Element ele = is.inside();
            std::vector<int> localSelfIndices = localOppositeIndicesDirect_(is, ele);
            std::pair<Element, std::vector<int>> pair(ele, localSelfIndices);
            eles.push_back(pair);
        }
        return eles;
    }

    std::vector<int> localOppositeIndicesDirect_(const Intersection& myIs, const Element& element) const 
    {
        std::vector<int> retVec;
        int localIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element))
        {
            bool isNotParallelSelf = false;
            bool isNotNormal = false;
            //TODO think about dim vs dimworld
            for (unsigned int direction = 0; direction < dim; ++direction)
            {
                //direction is normal to myIs
                if (direction == Dumux::directionIndex(std::move(myIs.centerUnitOuterNormal())))
                {
                    if(!scalarCmp(is.geometry().center()[direction], myIs.geometry().center()[direction]))
                    {
                        isNotParallelSelf = true;
                    }
                }
                //direction is parallel to myIs, in 3D this is two directions
                else
                {
                    int noCornerIndicator = 0;
                    for (unsigned int i=0; i < element.geometry().corners(); ++i){
                        if(!scalarCmp(is.geometry().center()[direction], element.geometry().corner(i)[direction]))
                        {
                            ++noCornerIndicator;
                        }
                    }
                    if (noCornerIndicator == element.geometry().corners())
                    {
                        isNotNormal = true;
                    }
                }
            }
            if (isNotParallelSelf == true && isNotNormal == true)
            {
                retVec.push_back(localIsIdx);
            }
            ++localIsIdx;
        }

        return retVec;
    }

    GlobalPosition average_center(const GlobalPosition pos1, const GlobalPosition pos2)
    {
        GlobalPosition pos;
        pos[0] = (pos1[0] + pos2[0]) / 2.0;
        pos[1] = (pos1[1] + pos2[1]) / 2.0;
        return pos;
    }

    /*!
     * \brief Returns the local opposing intersection index
     *
     * \param idx The local index of the intersection itself
     */
    std::vector<int> localOppositeIndices_(const int idx, const Element& element) const
    {
        const auto myIs = getFacet_(idx, element);
        return localOppositeIndicesDirect_(myIs, element);
    }

    std::vector<int> localSelfIndicesDirect_(const Intersection& myIs, const Element& element) const
    {
        std::vector<int> retVec;
        int localIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element))
        {
            //TODO think about dim vs dimworld
            for (unsigned int direction = 0; direction < dim; ++direction)
            {
                //direction is normal
                if (direction == Dumux::directionIndex(std::move(myIs.centerUnitOuterNormal())))
                {
                    if(scalarCmp(is.geometry().center()[direction], myIs.geometry().center()[direction]))
                    {
                        retVec.push_back(localIsIdx);
                    }
                }
            }
            ++localIsIdx;
        }

        return retVec;
    }

    //returns the index of self and of parallelSelf
    std::vector<int> localSelfIndices_(const int idx, const Element& element) const
    {
        const auto myIs = getFacet_(idx, element);

        return localSelfIndicesDirect_(myIs, element);
    }


    /*!
     * \brief Returns true if the intersection lies normal to another given intersection
     *
     * \param selfIdx The local index of the intersection itself
     * \param otherIdx The local index of the other intersection
     */
    bool facetIsNormal_(const int selfIdx, const int otherIdx, const Element& element) const
    {
        std::vector<int> localOppositeIndices = localOppositeIndices_(selfIdx, element);
        std::vector<int> localSelfIndices = localSelfIndices_(selfIdx, element);

        return !(
        std::find(localSelfIndices.begin(), localSelfIndices.end(), otherIdx) != localSelfIndices.end()
        ||
        std::find(localOppositeIndices.begin(), localOppositeIndices.end(), otherIdx) != localOppositeIndices.end()
        );
    }

    // non-directional: not in the direction of the normal vector of intersection_
    // true if the non-directional coordinate of one of the corners of intersection_ matches the
    // non-directional coordinate of the center of isWithCenter
    // is typically used in a case intersection_.inside().level() < intersection_.outside().level()
    //                     -------------
    //                    |            |
    //                    |            |warning:
    //                    |            |this opposite intersection also returns true
    //                    ||           |if there is a coarse neighbor -> needs to be
    //                    ||           |combined with utility_.facetIsNormal_ !!!
    //                    ||           |
    //                     ======-******
    //                    |      |     |
    //                    |      |     |
    //                    |      |     |
    //                     -------------
    // ||:myintersection, === and ***: isWithCenters which return true

    bool haveNonDirectionCornerCenterMatch_(const Intersection& isWithCenter)
    {
        //TODO figure out in 3D what I want

        bool retVal = false;

        const auto nonDirIndices = nonDirectionIndices();

        if (nonDirIndices.size() != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Not yet prepared for 3D.");
        }

        for (const auto nonDirectionIdx : nonDirIndices)
        {
            for (unsigned int i = 0; i < intersection_->corners(); ++i)
            {
                const auto cor = intersection_->corner(i);

                if (scalarCmp(cor[nonDirectionIdx], isWithCenter.geometry().center()[nonDirectionIdx]))
                {
                    retVal = true;
                }
            }
        }

        return retVal;
    }

    const Intersection findIntersectionWhichSharesACorner(std::vector<Intersection> intersections)
    {
        for (const auto is : intersections)
        {
            if (haveNonDirectionCenterCornerMatch_(is))
                return is;
        }
        //Warning elimination
        DUNE_THROW(Dune::InvalidStateException, "Should not happen.");
        return intersections[0];
    }

    bool haveNonDirectionCenterCornerMatchCVD_(CVDIntersectionBase<GridView, Intersection> *& isWithCorners)
    {
        bool retVal = false;

        for (unsigned int i = 0; i < isWithCorners->corners(); ++i)
        {
            const auto cor = isWithCorners->corner(i);

            if (scalarCmp(cor[directionIndex()], intersection_->center()[directionIndex()]))
            {
                retVal = true;
            }
        }

        return retVal;
    }

    bool haveNonDirectionCenterCornerMatch_(const Intersection& isWithCorners)
    {
        bool retVal = false;

        for (unsigned int i = 0; i < isWithCorners.geometry().corners(); ++i)
        {
            const auto cor = isWithCorners.geometry().corner(i);

            if (scalarCmp(cor[directionIndex()], intersection_->center()[directionIndex()]))
            {
                retVal = true;
            }
        }

        return retVal;
    }

    /*!
    * \brief Returns a cvdintersection with the same normal as intersection_ in the selected element
    * \param otherEle the element to look intersection for
    * \param intersectionMapper instance of the intersectionmapper
    */
    template <class IntersectionMapper>
    CVDIntersectionBase<GridView, Intersection> *findSameDirectionIntersectionAsIs(CVDIntersectionBase<GridView, Intersection> *&is_,
                                                                               Element &otherEle, IntersectionMapper &intersectionMapper)
    {
        auto ownIdx = outerNormalIndex(is_);
        for (auto otherIs : intersectionMapper->elementIntersections(otherEle))
        {
            auto otherIdx = outerNormalIndex(otherIs);
            if (ownIdx == otherIdx)
            {
                return otherIs;
            }
        }
        std::cout<<"Could not find same dir. intersection. Own idx: "<<ownIdx<<std::endl;
        for (auto otherIs : intersectionMapper->elementIntersections(otherEle))
        {
            auto otherIdx = outerNormalIndex(otherIs);
            std::cout<<"Other Idx: "<<otherIdx<<std::endl;
        }
        //Should not happen
        DUNE_THROW(Dune::InvalidStateException, "Could not find an intersection with same direction.");
        return nullptr;
    }

    /*!
    * \brief Returns a cvdintersection with the same normal as intersection_ in the selected element
    * \param otherEle the element to look intersection for
    * \param intersectionMapper instance of the intersectionmapper
    */
    template<class IntersectionMapper>
    CVDIntersectionBase<GridView, Intersection>* findSameDirectionIntersection(Element &otherEle, IntersectionMapper &intersectionMapper)
    {
        return findSameDirectionIntersectionAsIs(intersection_, otherEle, intersectionMapper);
    }

    Scalar quadraticInterpolationFactor_(Scalar ownDistance, Scalar otherDistance1, Scalar otherDistance2)
    {
        return otherDistance1 * otherDistance2 / ((otherDistance1 - ownDistance) * (otherDistance2 - ownDistance));
    }

    std::array<int, dim-1> nonDirectionIndices() const
    {
        std::array<int, dim-1> retArray; //initialize just to avoid a "may be used uninitialized" warning

        int vectorPos = 0;
        for (int i = 0; i < dim; ++i)
        {
            if (vectorPos <= dim-1)
            {
                if (i != directionIndex())
                {
                    retArray[vectorPos] = i;
                    ++vectorPos;
                }
            }
            else
            {
                DUNE_THROW(Dune::InvalidStateException, "Should not happen.");
            }
        }

        if (retArray.size() != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Not yet prepared for 3D.");
        }

        return retArray;
    }

    /*!
     * \brief Returns the dirction index of the primary facet (0 = x, 1 = y, 2 = z)
     */
    int directionIndex() const
    {
        return Dumux::directionIndex(std::move(intersection_->centerUnitOuterNormal()));
    }

    template<class IntersectionMapper>
    CVDIntersectionBase<GridView, Intersection>* findLocalOppositeIntersection(CVDIntersectionBase<GridView, Intersection>*& is_, IntersectionMapper mapper_){
        if(!is_->isSimple() && is_->isInsideFiner()){
            DUNE_THROW(Dune::InvalidStateException, "Cannot find local opposite intersection for CVDDoubleIntersection.");
        } else {
            for(auto is:mapper_->elementIntersections(is_->inside()))
            {
                if(isNormalOpposite(is_, is)){
                    return is;
                }
            }
        }
        return nullptr;
    }

    bool isNormalOpposite(CVDIntersectionBase<GridView, Intersection>*& is1, CVDIntersectionBase<GridView, Intersection>*& is2)
    {
        int idx1 = outerNormalIndex(is1);
        int idx2 = outerNormalIndex(is2);
        bool areNotEqual = idx1!=idx2;
        return areNotEqual && is1->directionIndex() == is2->directionIndex();
    }

    //Returns a unique index for every direction
    //left = 0
    //right = 1
    //down = 2
    //up = 3
    unsigned int outerNormalIndex(CVDIntersectionBase<GridView, Intersection>*& is){
        return is->outerNormalIndex();
    }

    Intersection getFacet_(const int localFacetIdx, const Element& element) const
    {
        Intersection returnIs;
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element)){
            if (localMyIsIdx == localFacetIdx)
            {
                returnIs = is;
                break;
            }
            ++localMyIsIdx;
        }
        return returnIs;        
    }


    template<class ScvfBuildingData>
    void fillIsTwoLevelDiagonal_(std::vector<int>& dofs,  std::vector<Scalar>& interpFacts, const Element& closestElement, const Element& smallerElement, const Element& sameSizedElement, const Element& largerElement, Scalar borderToLargerLength, Scalar borderToSmallerLength, Scalar smallerLonelyLength, Scalar largerLonelyLength, std::vector<ScvfBuildingData>& scvfBuildingData)
    {
        ScvfBuildingData emptyBuildingData;
        emptyBuildingData.eIdx = -1;
        emptyBuildingData.localScvfIdx = -1;

        dofs.push_back(gridView_.indexSet().index(closestElement));
        interpFacts.push_back((0.5*(1.0 - (0.5*borderToLargerLength)/(borderToLargerLength + 0.5*smallerLonelyLength))/*C, eq. (4.8)*/) *
                            (1.0 - (0.5*borderToSmallerLength)/(borderToSmallerLength + largerLonelyLength)/*A, eq. (4.9)*/));
        scvfBuildingData.push_back(emptyBuildingData);

        dofs.push_back(gridView_.indexSet().index(smallerElement));
        interpFacts.push_back(1.0 - (0.5*borderToLargerLength + 0.5*smallerLonelyLength)/(borderToLargerLength + 0.5*smallerLonelyLength)/*D, eq. (4.9)*/);
        scvfBuildingData.push_back(emptyBuildingData);

        dofs.push_back(gridView_.indexSet().index(largerElement));
        interpFacts.push_back((1.0 - (0.5*borderToLargerLength)/(borderToLargerLength + 0.5*smallerLonelyLength)/*C, eq. (4.8)*/) *
                            (1.0 - (largerLonelyLength + 0.5*borderToSmallerLength)/(borderToSmallerLength + largerLonelyLength)/*B, eq. (4.9)*/));
        scvfBuildingData.push_back(emptyBuildingData);

        dofs.push_back(gridView_.indexSet().index(sameSizedElement));
        interpFacts.push_back(0.5*(1.0 - (0.5*borderToLargerLength)/(borderToLargerLength + 0.5*smallerLonelyLength)/*C, eq. (4.8)*/) *
                            (1.0 - (0.5*borderToSmallerLength)/(borderToSmallerLength + largerLonelyLength)/*A, eq. (4.9)*/));
        scvfBuildingData.push_back(emptyBuildingData);
    }

};
}  // namespace Dumux
#endif
