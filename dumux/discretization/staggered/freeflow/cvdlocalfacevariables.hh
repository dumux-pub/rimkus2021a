// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDStaggeredLocalFaceVariables
 */
#ifndef DUMUX_DISCRETIZATION_STAGGERED_FREEFLOW_CVD_LOCAL_FACEVARIABLES_HH
#define DUMUX_DISCRETIZATION_STAGGERED_FREEFLOW_CVD_LOCAL_FACEVARIABLES_HH

#include <array>
#include <vector>

namespace Dumux
{

/*!
 * \ingroup StaggeredDiscretization
 * \brief The local face variables class for free flow staggered grid models.
 *        Contains all relevant velocities for the assembly of the momentum balance.
 *        This implementation is a specification for coarse-velocity discretization
 */
template<class FacePrimaryVariables>
class CVDStaggeredLocalFaceVariables
{
    using Scalar = typename FacePrimaryVariables::block_type;

public:

    /*!
    * \brief Complete update of the face variables (i.e. velocities for free flow)
    *        for a given face
    *
    * \param faceSol The face-specific solution vector
    * \param element The element
    * \param fvGeometry The finite-volume geometry
    * \param scvf The sub-control volume face of interest
    */
    template<class SolVector, class Element,
             class FVElementGeometry>
    void update(const SolVector& sol,
                const Element& element,
                const FVElementGeometry& fvGeometry)
    {
        eleLocalFaceVelocities_.resize(4);
        for (auto&& scvf : scvfs(fvGeometry))
        {
            int counter = 0;
            for(auto is:scvf.intersections())
            {
                if(is.inside() == element)
                {
                    auto faceVars = scvf.localVelVarsData()[counter];
                    auto outerNIdx = scvf.outerNormalIndex();
                    Scalar result = interpolate(sol, faceVars);
                    eleLocalFaceVelocities_[outerNIdx] = result;
                    break;
                }
                counter++;
            }
        }
    }


    template<class ElementFaceSolution, class Element,
             class FVElementGeometry>
    void updateWithDeflection(const ElementFaceSolution& sol,
                const Element& element,
                const FVElementGeometry& fvGeometry,
                const int& dofIdx,
                const Scalar& deflectedValue)
    {
        eleLocalFaceVelocities_.resize(4);
        for (auto&& scvf : scvfs(fvGeometry))
        {
            int counter = 0;
            for(auto is:scvf.intersections())
            {
                if(is.inside() == element)
                {
                    auto faceVars = scvf.localVelVarsData()[counter];
                    auto outerNIdx = scvf.outerNormalIndex();
                    Scalar result = interpolateWithDeflection(sol, faceVars, dofIdx, deflectedValue);
                    eleLocalFaceVelocities_[outerNIdx] = result;
                    break;
                }
                counter++;
            }
        }
    }

    Scalar velocity(const int& outerNormalIdx) const
    {
        return eleLocalFaceVelocities_[outerNormalIdx];
    }


private:
    template <class ElementFaceSolution, class LocalFaceVariableData>
    Scalar interpolateWithDeflection(const ElementFaceSolution &sol,
                                     LocalFaceVariableData &data,
                                     const int &dofIdx,
                                     const Scalar &deflectedValue)
    {
        Scalar sum = 0.0;
        for (int i = 0; i < data.dofs.size(); i++)
        {
            Scalar variable;
            if(data.dofs[i] == dofIdx)
                variable = deflectedValue;
            else
                variable = sol[data.dofs[i]][0];
            sum += variable * data.interpolationFactors[i];
        }
        return sum;
    }

    template<class SolVector, class LocalFaceVariableData>
    Scalar interpolate(const SolVector& sol, LocalFaceVariableData& data)
    {
        Scalar sum = 0.0;
        for(int i=0;i<data.dofs.size(); i++)
        {
            sum+=sol[data.dofs[i]] * data.interpolationFactors[i];
        }
        return sum;
    }

    std::vector<Scalar> eleLocalFaceVelocities_;

};

} // end namespace Dumux

#endif // DUMUX_DISCRETIZATION_STAGGERED_FREEFLOW_CVD_LOCAL_FACEVARIABLES_HH
