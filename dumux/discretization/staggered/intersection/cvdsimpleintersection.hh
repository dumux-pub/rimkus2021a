// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDSimpleIntersection
 */
#ifndef DUMUX_DISCRETIZATION_COARSE_VEL_DISC_SIMPLE_INTERSECTION_HH
#define DUMUX_DISCRETIZATION_COARSE_VEL_DISC_SIMPLE_INTERSECTION_HH

#include <dumux/discretization/staggered/cvdsubcontrolvolumeface.hh>
#include <dumux/discretization/staggered/intersection/cvdintersectionbase.hh>

namespace Dumux {

template <class GV,
          class Intersection>
class CVDSimpleIntersection : public CVDIntersectionBase<GV, Intersection> {
   public:

    using Scalar = typename GV::ctype;
    using GlobalPosition = Dune::FieldVector<Scalar, GV::dimensionworld>;
    using Element = typename GV::template Codim<0>::Entity;

    CVDSimpleIntersection(int globalIntersectionIndex, const Intersection& intersection)
        : CVDIntersectionBase<GV, Intersection> (globalIntersectionIndex),
        intersection_(intersection) {}

    bool neighbor(){
        return intersection_.neighbor();
    }

    bool boundary(){
        return intersection_.boundary();
    }

    bool isSimple(){
        return true;
    }

    bool isInsideFiner()
    {
        return neighbor() && intersection_.inside().level() > intersection_.outside().level();
    }

    bool isOutsideFiner()
    {
        return neighbor() && intersection_.outside().level() > intersection_.inside().level();
    }

    std::vector<Intersection> intersections() const{
        return {intersection_};
    }

    Dune::GeometryType geomType(){
        return intersection_.geometry().type();
    }

    Scalar volume(){
        return intersection_.geometry().volume();
    }

    const Element outside(){
        return intersection_.outside();
    }

    const Element inside(){
        return intersection_.inside();
    }

    GlobalPosition center(){
        return intersection_.geometry().center();
    }

    GlobalPosition centerUnitOuterNormal() const {
        return intersection_.centerUnitOuterNormal();
    }

    std::vector<GlobalPosition> cornersvec() {
        std::vector<GlobalPosition> corners_;
        corners_.resize(intersection_.geometry().corners());
        for (int i = 0; i < intersection_.geometry().corners(); ++i)
            corners_[i] = intersection_.geometry().corner(i);
        return corners_;
    }

    GlobalPosition corner(const int& i){
        return cornersvec()[i];
    }

    unsigned int corners(){
        return intersection_.geometry().corners();
    }

    const Intersection getIntersection(){
        return intersection_;
    }

    private:
    const Intersection intersection_;
};

}  // end namespace Dumux

#endif
