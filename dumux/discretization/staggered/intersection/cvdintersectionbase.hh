// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDIntersectionBase
 */
#ifndef DUMUX_DISCRETIZATION_COARSE_VEL_DISC_INTERSECTION_BASE_HH
#define DUMUX_DISCRETIZATION_COARSE_VEL_DISC_INTERSECTION_BASE_HH

#include <dumux/discretization/staggered/cvdsubcontrolvolumeface.hh>

namespace Dumux
{

    template <class GV,
              class Intersection>
    class CVDIntersectionBase
    {

    private:
        int globalIntersectionIndex_;

    public:
        using Scalar = typename GV::ctype;
        using GlobalPosition = Dune::FieldVector<Scalar, GV::dimensionworld>;
        using Element = typename GV::template Codim<0>::Entity;

        CVDIntersectionBase(int globalIntersectionIndex)
        :globalIntersectionIndex_(globalIntersectionIndex)
        {}

        virtual bool neighbor() = 0;

        virtual bool boundary() = 0;

        virtual bool isSimple() = 0;

        virtual bool isInsideFiner() = 0;

        virtual bool isOutsideFiner() = 0;

        virtual std::vector<Intersection> intersections() const = 0;

        virtual Dune::GeometryType geomType() = 0;

        virtual Scalar volume() = 0;

        virtual const Element outside() = 0;

        virtual const Element inside() = 0;

        virtual GlobalPosition center() = 0;

        virtual GlobalPosition centerUnitOuterNormal() const = 0;

        //Method corners() reserved to change geometry().corners()
        virtual std::vector<GlobalPosition> cornersvec() = 0;

        virtual GlobalPosition corner(const int &i) = 0;

        virtual unsigned int corners() = 0;

        int refinementLevel()
        {
            int lvl = 0;
            for(auto is:intersections())
            {
                lvl = std::max(lvl, is.inside().level());
            }
            return lvl;
        }

        int directionIndex()
        {
            return Dumux::directionIndex(std::move(centerUnitOuterNormal()));
        }

        int nonDirectionIndex()
        {
            int dirIdx = directionIndex();
            if(dirIdx == 0)
                return 1;
            else
                return 0;
        }

        bool isNormal(CVDIntersectionBase<GV, Intersection> *&other)
        {
            auto selfIdx = Dumux::directionIndex(std::move(centerUnitOuterNormal()));
            auto otherIdx = Dumux::directionIndex(std::move(other->centerUnitOuterNormal()));
            return selfIdx != otherIdx;
        }

        int globalIntersectionIndex()
        {
            return globalIntersectionIndex_;
        }

        int outerNormalIndex()
        {
            unsigned int dirIdx = directionIndex();
            const auto &normal = centerUnitOuterNormal();
            bool directionPositive = normal[0] + normal[1] > 0.0;
            if (dirIdx == 0)
            {
                if (directionPositive)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else if (dirIdx == 1)
            {
                if (directionPositive)
                {
                    return 3;
                }
                else
                {
                    return 2;
                }
            }
            else if (dirIdx == 2)
            {
                DUNE_THROW(Dune::InvalidStateException, "Adaptive not prepared for three dimensions.");
            }
            return 0; //compiler warning ignore
        }

        /*!
    * \brief Takes pos and replaces pos[directionIndex] with center()[directionIndex] of this intersection
    * All other coordinates taken from \param pos
    */
        GlobalPosition project(GlobalPosition pos)
        {
            GlobalPosition ret = pos;
            int dirIdx = directionIndex();
            ret[dirIdx] = center()[dirIdx];
            return ret;
        }
    };

} // end namespace Dumux

#endif
