// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDIntersectionBase
 */
#ifndef DUMUX_DISCRETIZATION_COARSE_VEL_DISC_COARSE_DOUBLE_INTERSECTION_HH
#define DUMUX_DISCRETIZATION_COARSE_VEL_DISC_COARSE_DOUBLE_INTERSECTION_HH

#include <dumux/discretization/staggered/cvdsubcontrolvolumeface.hh>
#include <dumux/discretization/staggered/intersection/cvdintersectionbase.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdgeometryhelpergenericmethods.hh>

namespace Dumux {

template <class GV,
          class Intersection>
class CVDDoubleIntersection : public CVDIntersectionBase<GV, Intersection> {
   public:

    using Scalar = typename GV::ctype;
    using GlobalPosition = Dune::FieldVector<Scalar, GV::dimensionworld>;
    using Element = typename GV::template Codim<0>::Entity;

    CVDDoubleIntersection(int globalIntersectionIndex, const Intersection& intersectionOne, const Intersection& intersectionTwo)
        : CVDIntersectionBase<GV, Intersection> (globalIntersectionIndex),
          intersectionLevelOne_(intersectionOne),
          intersectionLevelTwo_(intersectionTwo) {}

    bool neighbor(){
        return intersectionLevelOne_.neighbor();
    }

    bool boundary(){
        //should never be boundary!
        return false;
    }

    bool isSimple(){
        return false;
    }

    bool isInsideFiner()
    {
        return neighbor() &&
               intersectionLevelOne_.inside().level() > intersectionLevelOne_.outside().level() &&
               intersectionLevelTwo_.inside().level() > intersectionLevelTwo_.outside().level();
    }

    bool isOutsideFiner()
    {
        return neighbor() &&
               intersectionLevelOne_.outside().level() > intersectionLevelOne_.inside().level() &&
               intersectionLevelTwo_.outside().level() > intersectionLevelTwo_.inside().level();
    }

    std::vector<Intersection> intersections() const{
        return {intersectionLevelOne_, intersectionLevelTwo_};
    }

    Dune::GeometryType geomType(){
        return intersectionLevelOne_.geometry().type();
    }

    Scalar volume(){
        Scalar volume = intersectionLevelOne_.geometry().volume() + intersectionLevelTwo_.geometry().volume();
        return volume;
    }

    const Element outside(){
        return intersectionLevelOne_.outside();
    }

    const Element inside(){
        return intersectionLevelOne_.inside();
    }

    GlobalPosition center(){
        return average_center();
    }

    GlobalPosition centerUnitOuterNormal() const {
        return intersectionLevelOne_.centerUnitOuterNormal();
    }

    std::vector<GlobalPosition> cornersvec()
    {
        std::vector<GlobalPosition> corners_;
        GlobalPosition center = this->center();
        corners_.clear();
        int corners1 = intersectionLevelOne_.geometry().corners();
        for (int i = 0; i < corners1; ++i)
        {
            auto corner = intersectionLevelOne_.geometry().corner(i);
            if(!containerCmp(center, corner)){
                corners_.push_back(corner);
            }
        }
        for (int i = 0; i < intersectionLevelTwo_.geometry().corners(); ++i)
        {
            auto corner = intersectionLevelTwo_.geometry().corner(i);
            if(!containerCmp(center, corner)){
                corners_.push_back(corner);
            }
        }
        return corners_;
    }

    GlobalPosition corner(const int& i){
        return cornersvec()[i];
    }

    unsigned int corners(){
        return 2;//(intersectionLevelOne_.geometry().corners() + intersectionLevelTwo_.geometry().corners());
    }

    Intersection first(){
        return intersectionLevelOne_;
    }

    Intersection second(){
        return intersectionLevelTwo_;
    }

    private:
    const Intersection intersectionLevelOne_;
    const Intersection intersectionLevelTwo_;

    GlobalPosition average_center()
    {
        GlobalPosition pos1 = intersectionLevelOne_.geometry().center();
        GlobalPosition pos2 = intersectionLevelTwo_.geometry().center();
        GlobalPosition pos;
        pos[0] = (pos1[0] + pos2[0]) / 2.0;
        pos[1] = (pos1[1] + pos2[1]) / 2.0;
        return pos;
    }
};

}  // end namespace Dumux

#endif
