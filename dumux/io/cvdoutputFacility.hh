// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesModel
 */

#ifndef DUMUX_CVD_IO_OUTPUTFACILITY_HH
#define DUMUX_CVD_IO_OUTPUTFACILITY_HH

#include <dumux/common/timeloop.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dumux/io/grid/cvdcontrolvolumegrids.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdstaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/cvdffsubcontrolvolumeface.hh>
#include <dumux/freeflow/navierstokes/cvdAnalyticalSolutionIntegration.hh>
#include <dumux/io/cvdreaddofbasedresult.hh>

namespace Dumux{
template<class TypeTag>
class CVDCVOutputFacility
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using CellCenterSolutionVector = GetPropType<TypeTag, Properties::CellCenterSolutionVector>;
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
    using MLGTraits = typename CVDFreeFlowStaggeredDefaultScvfGeometryTraits<GridView>::template ScvfMLGTraits<Scalar> ;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using HostGridManager = GridManager<HostGrid>;
    using CVGridGeometry = GetPropType<TypeTag, Properties::CVGridGeometry>;
    using CVElement = typename CVGridGeometry::GridView::template Codim<0>::Entity;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Assembler = CVDStaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;

public:
    /*!
     * \brief The Constructor
     */
    CVDCVOutputFacility(std::shared_ptr<const Problem> problem)
    : problem_(problem)
    {}

    void iniCVGridManagers(std::array<HostGridManager, 2>& gridManagers,
                           std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>, 2>& cvCenterScvfIndicesMaps,
                           const std::array<std::string, 2>& paramGroups) const
    {
        // generate dgf files, initialize grid managers
        cvCenterScvfIndicesMaps[0] = generateCVsDGFfile<GridGeometry, GlobalPosition>(paramGroups[0], (*problem_).gridGeometry(), 0);
        cvCenterScvfIndicesMaps[1] = generateCVsDGFfile<GridGeometry, GlobalPosition>(paramGroups[1], (*problem_).gridGeometry(), 1);

        std::string testString = getParam<std::string>("finexCVs.Grid.File", "");
        if (testString == "")
            DUNE_THROW(Dune::InvalidStateException, "Please set finexCVs.Grid.File.");

        for (unsigned int i = 0; i < 2; ++i)
            gridManagers[i].init(paramGroups[i]);
    }

    void onCVsOutputResidualsAndPrimVars(std::shared_ptr<const GridGeometry> gridGeometry,
        std::array<HostGridManager, 2>& cvGridManagers,
        const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>, 2>& cvCenterScvfIndicesMaps,
        const std::array<std::string, 2>& paramGroups,
        const SolutionVector& sol,
        unsigned int timeIndex,
        const FaceSolutionVector& faceResidualWithAnalyticalSol,
        const std::optional<std::array<std::vector<Scalar>, 2>>& perfectInterpolationFaceResiduals,
        bool readDofBasedValues) const
    {
        std::array<std::vector<std::vector<Scalar>>, 2> dualIndexedOutputVecsArray = {};
        std::vector<std::string> coarseOutputVecNames;

        //
        {
            std::vector<FaceSolutionVector> primalIndexOutputVecs;

            const FaceSolutionVector& vel = sol[GridGeometry::faceIdx()];
            primalIndexOutputVecs.push_back(vel);
            coarseOutputVecNames.push_back("velocity");

            const FaceSolutionVector& anaVel = problem_->getAnalyticalFaceSolutionVector();
            primalIndexOutputVecs.push_back(anaVel);
            coarseOutputVecNames.push_back("analyticalVelocity");

            FaceSolutionVector absVelocityError;
            absVelocityError.resize((*problem_).gridGeometry().numIntersections());
            for (unsigned int i = 0; i < vel.size(); ++i)
            {
                absVelocityError[i] = std::abs(vel[i] - anaVel[i]);
            }
            primalIndexOutputVecs.push_back(absVelocityError);
            coarseOutputVecNames.push_back("absVelocityError");

            if (readDofBasedValues)
            {
                FaceSolutionVector IBAMRvel;
                IBAMRvel.resize((*problem_).gridGeometry().numFaceDofs());

                ReadDofBasedResult<TypeTag> reader(problem_);
                reader.readDofBasedResult(IBAMRvel, "velocity");
                primalIndexOutputVecs.push_back(IBAMRvel);
                coarseOutputVecNames.push_back("IBAMRvel");
            }

            primalIndexOutputVecs.push_back(faceResidualWithAnalyticalSol);
            coarseOutputVecNames.push_back("numericalMomResAnalytical");

            std::array<std::vector<std::vector<Scalar>>,2> dualIndexOutputVec = onePrimalIndexedVectorToTwoDualIndexedVectors_(primalIndexOutputVecs, cvGridManagers, cvCenterScvfIndicesMaps);
            for (unsigned int i = 0; i < 2; ++i)
                for (unsigned int j = 0; j < dualIndexOutputVec[i].size(); ++j)
                    dualIndexedOutputVecsArray[i].push_back(dualIndexOutputVec[i][j]);
        }

        if (perfectInterpolationFaceResiduals)
        {
            for (unsigned int i = 0; i < 2; ++i)
                dualIndexedOutputVecsArray[i].push_back((*perfectInterpolationFaceResiduals)[i]);
            coarseOutputVecNames.push_back("perfectInterpMomResWithAnalytical");
        }

        std::array<std::vector<std::string>, 2> outputVecNames;
        for (unsigned int i = 0; i < coarseOutputVecNames.size(); ++i)
        {
            outputVecNames[0].push_back(coarseOutputVecNames[i]+"_x");
            outputVecNames[1].push_back(coarseOutputVecNames[i]+"_y");
        }

        for (unsigned int i = 0; i < 2; ++i)
        {
            using CVsGridView = std::decay_t<decltype(cvGridManagers[i].grid().leafGridView())>;
            Dune::VTKWriter<CVsGridView> writer (cvGridManagers[i].grid().leafGridView());
            for (unsigned int j = 0; j < dualIndexedOutputVecsArray[i].size(); ++j)
                writer.addCellData(dualIndexedOutputVecsArray[i][j], outputVecNames[i][j]);

            std::string timeIndexString = std::to_string(timeIndex);
            writer.write(paramGroups[i]+"-0000"+timeIndexString);
        }
    }

    void printPvdFiles(const std::array<std::string, 2>& paramGroups,
        const std::vector<std::pair<unsigned int, Scalar>> pvdFileInfos)
    {
        for (unsigned int i = 0; i < 2; ++i)
        {
            std::string name = paramGroups[i];

            std::ofstream dgfFile;
            dgfFile.open(name + ".pvd");
            dgfFile << "<?xml version=\"1.0\"?>"  << std::endl;
            dgfFile << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">"  << std::endl;
            dgfFile << "<Collection>" << std::endl;

            for (const auto& pvdFileInfo : pvdFileInfos)
                dgfFile << "<DataSet timestep=\"" << pvdFileInfo.second << "\" group=\"\" part=\"0\" name=\"\" file=\""<< name << "-0000" << pvdFileInfo.first << ".vtu\"/>"  << std::endl;
            dgfFile << "</Collection>" << std::endl;
            dgfFile << "</VTKFile>" << std::endl;
            dgfFile.close();
        }
    }

private:
    /*!
     * \brief scvfDofIndex from control volume
     *
     * \param cv The control volume
     */
    unsigned int scvfDofIndexFromCv_(const CVElement& cv,
        const std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>& cvCenterScvfIndicesMap) const
    {
        auto it = cvCenterScvfIndicesMap.find(cv.geometry().center());
        std::vector<unsigned int> scvfIndices = (*it).second;
        SubControlVolumeFace scvf = (*problem_).gridGeometry().scvf(scvfIndices[0]);

        return scvf.dofIndex();
    }

    /*!
     * \brief control volume index from control volume
     *
     * \param cv The control volume
     */
    unsigned int cvIndexFromCv_(const CVElement& cv,
        const HostGrid::LeafGridView& cvLeafGridView) const
    {
        return cvLeafGridView.indexSet().index(cv);
    }

    template<class OutputVecs>
    std::vector<std::vector<Scalar>> onePrimalIndexedVectorToOneDualIndexedVectorInOneDirection_(unsigned int dirIdx,
        OutputVecs& primalIndexOutputVecs,
        std::array<HostGridManager, 2>& gridManagers,
        const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>, 2>& cvCenterScvfIndicesMaps) const
    {
        const HostGrid::LeafGridView& cVsLeafGridView = (gridManagers[dirIdx]).grid().leafGridView();

        std::vector<std::vector<Scalar>> modifiedprimalIndexOutputVecs;
        std::vector<Scalar> dummyVec;
        dummyVec.resize(cVsLeafGridView.size(0));
        for (unsigned int i = 0; i < primalIndexOutputVecs.size(); ++i)
            modifiedprimalIndexOutputVecs.push_back(dummyVec);

        for (const auto& cv : elements(cVsLeafGridView))
        {
            for (unsigned int i = 0; i < primalIndexOutputVecs.size(); ++i)
                modifiedprimalIndexOutputVecs[i][cvIndexFromCv_(cv, cVsLeafGridView)] = primalIndexOutputVecs[i][scvfDofIndexFromCv_(cv, cvCenterScvfIndicesMaps[dirIdx])];
        }

        return modifiedprimalIndexOutputVecs;
    }

    template<class OutputVecs>
    std::array<std::vector<std::vector<Scalar>>,2> onePrimalIndexedVectorToTwoDualIndexedVectors_(OutputVecs& primalIndexOutputVecs,
         std::array<HostGridManager, 2>& gridManagers,
        const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>, 2>& cvCenterScvfIndicesMaps) const
    {
        std::vector<std::vector<Scalar>> modifiedprimalIndexOutputVecsX = onePrimalIndexedVectorToOneDualIndexedVectorInOneDirection_(0, primalIndexOutputVecs, gridManagers, cvCenterScvfIndicesMaps);
        std::vector<std::vector<Scalar>> modifiedprimalIndexOutputVecsY = onePrimalIndexedVectorToOneDualIndexedVectorInOneDirection_(1, primalIndexOutputVecs, gridManagers, cvCenterScvfIndicesMaps);

        std::array<std::vector<std::vector<Scalar>>,2> retArray = {modifiedprimalIndexOutputVecsX, modifiedprimalIndexOutputVecsY};

        return retArray;
    }

    std::shared_ptr<const Problem> problem_;
};
} // end namespace Dumux

#endif
