// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUMUX_CVD_IO_HH
#define DUMUX_CVD_IO_HH

#include <cmath>
#include <complex>
#include <limits>
#include <ios>
#include <iomanip>
#include <fstream>
#include <string>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/istl/bcrsmatrix.hh>


namespace Dune {

  /**
   * \brief Prints a BCRSMatrix with fixed sized blocks.
   *
   * \code
   * #include <dune/istl/io.hh>
   * \endcode
   *
   * Only the nonzero entries will be printed as matrix blocks
   * together with their
   * corresponding column index and all others will be omitted.
   *
   * This might be preferable over printmatrix in the case of big
   * sparse matrices with nonscalar blocks.
   *
   * @param s The ostream to print to.
   * @param mat The matrix to print.
   * @param width The number of nonzero blocks to print in one line.
   * @param precision The precision to use when printing the numbers.
   */
  template<class B, int n, int m, class A>
  void writeSparseMatrixToCSV(std::ostream& s,
                        const BCRSMatrix<FieldMatrix<B,n,m>,A>& mat,
                        int precision=3)
  {
    typedef BCRSMatrix<FieldMatrix<B,n,m>,A> Matrix;
    // remember old flags
        
    std::ios_base::fmtflags oldflags = s.flags();
    
    // set the output format
    s.setf(std::ios_base::scientific, std::ios_base::floatfield);
    int oldprec = s.precision();
    s.precision(precision);

    typedef typename Matrix::ConstRowIterator Row;

    double array[mat.N()][mat.M()] = {};
    for (Row row = mat.begin(); row != mat.end(); ++row)
    {
        int count = 0;
        typedef typename Matrix::ConstColIterator Col;
        Col col = row->begin();
        for (; col != row->end(); ++col, ++count)
        {
            array[row.index()][col.index()] = (*col)[row.index()][col.index()];            
        }
    }

    for(int i =0; i<mat.N(); i++)
    {
        for(int j=0; j<mat.M(); j++)
        {
            s<<array[i][j];
            if(j==mat.M()-1)
                s<<std::endl;
            else
                s<<",";
        }
    }

    // reset the output format
    s.flags(oldflags);
    s.precision(oldprec);    
  }

  /**
   * \brief Prints a BCRSMatrix with fixed sized blocks.
   *
   * \code
   * #include <dune/istl/io.hh>
   * \endcode
   *
   * Only the nonzero entries will be printed as matrix blocks
   * together with their
   * corresponding column index and all others will be omitted.
   *
   * This might be preferable over printmatrix in the case of big
   * sparse matrices with nonscalar blocks.
   *
   * @param s The ostream to print to.
   * @param mat The matrix to print.
   * @param width The number of nonzero blocks to print in one line.
   * @param precision The precision to use when printing the numbers.
   */
  template<class Vector>
  void writeVectorToCSV(std::ostream& s,
                        const Vector& vec,
                        int precision=4)
  {
    // remember old flags
        
    std::ios_base::fmtflags oldflags = s.flags();
    
    // set the output format
    s.setf(std::ios_base::scientific, std::ios_base::floatfield);
    int oldprec = s.precision();
    s.precision(precision);

    for (int i = 0; i < vec.N(); i++)
    {
        s << vec[i];
        if (i < vec.N() - 1)
            s << std::endl;            
    }

    // reset the output format
    s.flags(oldflags);
    s.precision(oldprec);    
  }

} // namespace Dune

#endif
