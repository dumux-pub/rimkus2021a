// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Common
 * \brief defines intersection mappers.
 */
#ifndef DUMUX_COARSE_VEL_DISC_INTERSECTIONITERATOR_HH
#define DUMUX_COARSE_VEL_DISC_INTERSECTIONITERATOR_HH

#include <numeric>
#include <vector>
#include <unordered_map>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/common/rangegenerators.hh>
#include <dumux/common/intersectionmapper.hh>
#include <dumux/discretization/staggered/intersection/cvdintersectionbase.hh>
#include <dumux/discretization/staggered/intersection/cvdsimpleintersection.hh>
#include <dumux/discretization/staggered/intersection/cvddoubleintersection.hh>


namespace Dumux {

/*!
 * \ingroup Common
 * \brief defines an intersection mapper for mapping of global DOFs assigned
 *        to faces which also works for non-conforming grids and corner-point grids.
 */
template<class GridView>
class CVDNonConformingGridIntersectionMapper
{
    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;
    using GridIndexType = typename GridView::IndexSet::IndexType;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    using Scalar = typename GridView::ctype;
    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

public:
    CVDNonConformingGridIntersectionMapper(const GridView& gridview)
    : gridView_(gridview),
        indexSet_(&gridView_.indexSet()),
        numIntersections_(gridView_.size(1)),
        intersectionMapGlobal_(gridView_.size(0))
    {

    }

    //! intersection.indexInInside does not give an intersection index, but a facetIndex. E.g. in 2D the rectangular elements have 0,1,2,3 as indexInInside. This function is intended to, in the case of hanging nodes, also have e.g. 4 as possible indexInInside - considering intersections instead of facets.
    GridIndexType cvdisIndexInInside(CVDIntersectionBase<GridView, Intersection>*& myIs) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, myIs->inside())){
            if (myIs->center() == is.geometry().center())
            {
                return localMyIsIdx;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in cvdisIndexInInside in intersectionmapper!" << std::endl;
        return 0;//just to avoid compiler warning
    }

    GridIndexType isIndexInInside(const Intersection& myIs) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, myIs.inside())){
            if (myIs.geometry().center() == is.geometry().center())
            {
                return localMyIsIdx;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in isIndexInInside in intersectionmapper!" << std::endl;
        return 0;//just to avoid compiler warning
    }

    GridIndexType cvdisIndexInOutside(CVDIntersectionBase<GridView, Intersection>*& myIs) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, myIs->outside())){
            if (containerCmp(myIs->center(), is.geometry().center()))
            {
                return localMyIsIdx;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in cvdisIndexInOutside in intersectionmapper!" << std::endl;
        return 0;//just to avoid compiler warning
    }

    GridIndexType isIndexInOutside(const Intersection& myIs) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, myIs.outside())){
            if (containerCmp(myIs.geometry().center(), is.geometry().center()))
            {
                return localMyIsIdx;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in isIndexInOutside in intersectionmapper!" << std::endl;
        return 0;//just to avoid compiler warning
    }

    //! The total number of intersections
    std::size_t numIntersections() const
    {
        return numIntersections_;
    }

    GridIndexType globalIntersectionIndex(const Element& element, const std::size_t localFaceIdx) const
    {
        return (intersectionMapGlobal_[index(element)].find(localFaceIdx))->second; //use find() for const function!
    }

    std::size_t numFaces(const Element& element)
    {
        return intersectionMapGlobal_[index(element)].size();
    }

    const auto velocityXPositions() const
    {
        return velocityXPositions_;
    }

    const auto velocityYPositions() const
    {
        return velocityYPositions_;
    }

    [[deprecated]]
    void updateOld()
    {
        intersectionMapGlobal_.clear();
        intersectionMapGlobal_.resize(gridView_.size(0));

        velocityXPositions_.clear();
        velocityYPositions_.clear();

        GridIndexType globalIntersectionIdx = 0;
        for (const auto& element : elements(gridView_))
        {
            int eIdx = index(element);
            int fIdx = 0;

            // run through all intersections with neighbors
            for (const auto& intersection : intersections(gridView_, element))
            {

                if (intersection.neighbor()) {
                    auto neighbor = intersection.outside();
                    int eIdxN = index(neighbor);
                    CVDSimpleIntersection<GridView, Intersection>* cvdIntersection = new CVDSimpleIntersection<GridView, Intersection>(intersection);
                    if (element.level() > neighbor.level() || (element.level() == neighbor.level() && eIdx < eIdxN)) {
                        int fIdxN = findNeighbourIntersectionIndex(neighbor, element);
                        intersectionMapGlobal_[eIdx][fIdx] = globalIntersectionIdx;
                        intersectionMapGlobal_[eIdxN][fIdxN] = globalIntersectionIdx;

                        setVelocityPositions(intersection, globalIntersectionIdx, intersection.geometry().center());

                        globalIntersectionIdx++;
                    }
                    elementIntersections_[eIdx].push_back(cvdIntersection);
                } else {
                    CVDSimpleIntersection<GridView, Intersection>* cvdIntersection = new CVDSimpleIntersection<GridView, Intersection>(intersection);

                    intersectionMapGlobal_[eIdx][fIdx] = globalIntersectionIdx;

                    setVelocityPositions(intersection, globalIntersectionIdx, intersection.geometry().center());

                    globalIntersectionIdx++;
                    elementIntersections_[eIdx].push_back(cvdIntersection);
                }

                fIdx++;
            }
        }
        numIntersections_ = globalIntersectionIdx;
    }

    void update (const GridView& gridView)
    {
        gridView_ = gridView;
        indexSet_ = &gridView_.indexSet();
        update_();
    }

    void update (GridView&& gridView)
    {
        gridView_ = std::move(gridView);
        indexSet_ = &gridView_.indexSet();
        update_();
    }

    void update_()
    {
        intersectionMapGlobal_.clear();
        intersectionMapGlobal_.resize(gridView_.size(0));

        velocityXPositions_.clear();
        velocityYPositions_.clear();

        GridIndexType globalIntersectionIdx = 0;
        for (const auto& element : elements(gridView_))
        {
            std::vector<CVDIntersectionBase<GridView, Intersection>*> cvdIntersections;

            int eIdx = index(element);
            int fIdx = 0;

            // auto eCtr = element.geometry().center();
            // std::cout<<"Element "<<eIdx<<" ["<<eCtr[0]<<","<<eCtr[1]<<"]"<<std::endl;

            // Run through all intersections with neighbors
            // Find pairs of intersections (for the same normal)
            // Array of vectors for each direction(2 for x, 2 for y)
            std::vector <Intersection> elementIntersections[4];
            for (const auto& intersection : intersections(gridView_, element))
            {
                //map of outernormal indices with corresposing intersections
                unsigned int outerNormalIdx = outerNormalIndex(intersection);
                elementIntersections[outerNormalIdx].push_back(intersection);
            }
            // Iterate through elementIntersections array
            // If the vector has 2 Intersections in the same direction, it's a DoubleIntersection
            for(const std::vector<Intersection>& intersectionList : elementIntersections){
                auto interCount = intersectionList.size();
                if (interCount == 1) {
                    const auto& intersection = intersectionList.at(0);


                    CVDSimpleIntersection<GridView, Intersection>* cvdIntersection = new CVDSimpleIntersection<GridView, Intersection>(globalIntersectionIdx, intersection);
                    CVDSimpleIntersection<GridView, Intersection>* nbIntersection = nullptr;
                    //Must create a  cvdintersection for neighbor with the same globalIntersectionIndex!
                    if(cvdIntersection->neighbor())
                    {
                        const auto& outerIntersection = outerIs(intersection);
                        nbIntersection = new CVDSimpleIntersection<GridView, Intersection>(globalIntersectionIdx, outerIntersection);
                    }

                    //Set the global intersection indices
                    setIndicesForIntersectionAndItsNeighbour(intersection, element, eIdx, fIdx, globalIntersectionIdx);
                    fIdx++;

                    //Push local cvdintersection regardless if a new global dof is set or not
                    //TODO: Ignore if it is part of a shared-dof for refined-to-coarse interface
                    if(!cvdIntersection->isInsideFiner() &&
                     (cvdIntersection->boundary() || eIdx < index(intersection.outside())))
                    {
                        elementIntersections_[eIdx].push_back(cvdIntersection);
                        if(nbIntersection != nullptr)
                        {
                            int eIdxN = index(intersection.outside());
                            elementIntersections_[eIdxN].push_back(nbIntersection);
                        }
                    }
                    else
                    {
                        //Is an intersection that belongs to a doublecvdintersection. Handled below.
                    }
                } else if (interCount == 2) {
                    const auto& intersectionOne = intersectionList.at(0);
                    const auto& intersectionTwo = intersectionList.at(1);
                    CVDDoubleIntersection<GridView, Intersection>* cvdIntersection = new CVDDoubleIntersection<GridView, Intersection>(globalIntersectionIdx, intersectionOne, intersectionTwo);


                    //Also create a double cvdIntersection for the two outside() elements.
                    const auto& outsideIsOne = outerIs(intersectionOne);
                    const auto& outsideIsTwo = outerIs(intersectionTwo);

                    CVDDoubleIntersection<GridView, Intersection>* fineSideIsOne = new CVDDoubleIntersection<GridView, Intersection>(globalIntersectionIdx, outsideIsOne, outsideIsTwo);
                    CVDDoubleIntersection<GridView, Intersection>* fineSideIsTwo = new CVDDoubleIntersection<GridView, Intersection>(globalIntersectionIdx, outsideIsTwo, outsideIsOne);

                    //Set indices for intersection and its neighbour
                    setIndicesForDoubleIntersectionAndItsNeighbour(cvdIntersection, element, eIdx, fIdx, globalIntersectionIdx);
                    fIdx+=2;

                    //Push local cvdintersection regardless if a new global dof is set or not
                    elementIntersections_[eIdx].push_back(cvdIntersection);
                    pushElementIntersection(outsideIsOne.inside(), fineSideIsOne);
                    pushElementIntersection(outsideIsTwo.inside(), fineSideIsTwo);

                } else if(interCount != 0) {
                    DUNE_THROW(Dune::InvalidStateException, "Refinement ratio between cells is higher than 2!");
                }
            }
            // elementIntersections_[eIdx] = cvdIntersections;
        }
        //Resort element intersection lists based on outerNormalIndex
        for(auto& pair:elementIntersections_)
        {
            sort(pair.second.begin(), pair.second.end(), [&](CVDIntersectionBase<GridView, Intersection>* &a, CVDIntersectionBase<GridView, Intersection>* &b)
             { return (outerNormalIndex(a->intersections()[0]) < outerNormalIndex(b->intersections()[0])); });
        }
        numIntersections_ = globalIntersectionIdx;
    }

    std::vector<CVDIntersectionBase<GridView, Intersection>*> elementIntersections(const Element& ele)
    {
        return elementIntersections_[index(ele)];
    }

    CVDIntersectionBase<GridView, Intersection>* elementIntersection(const Element& ele, const int localFaceIdx)
    {
        return elementIntersections_[index(ele)][localFaceIdx];
    }

private:
    GridIndexType index(const Element& element) const
    {
        return indexSet_->index(element);
    }

    void pushElementIntersection(const Element& element, CVDDoubleIntersection<GridView, Intersection>* is)
    {
        elementIntersections_[index(element)].push_back(is);
    }

    const Intersection outerIs(const Intersection& is)
    {
        for (const auto& intersection : intersections(gridView_, is.outside()))
        {
            if (containerCmp(is.geometry().center(), intersection.geometry().center()))
            {
                return intersection;
            }
        }
        DUNE_THROW(Dune::InvalidStateException, "Should not be outside of loop in outerIs_!");
        return is;
    }

    void setIndicesForDoubleIntersectionAndItsNeighbour(CVDDoubleIntersection<GridView, Intersection>* intersection,
                                                        const Element& element,
                                                        unsigned int eIdx,
                                                        unsigned int fIdx,
                                                        unsigned int& globalIntersectionIdx) {
        const auto& intersectionOne = intersection->first();
        const auto& intersectionTwo = intersection->second();

        if (intersectionOne.neighbor() && intersectionTwo.neighbor()) {
            auto neighborOne = intersectionOne.outside();
            auto neighborTwo = intersectionTwo.outside();
            int eIdxN1 = index(neighborOne);
            int eIdxN2 = index(neighborTwo);
            if (element.level() < neighborOne.level() && element.level() < neighborTwo.level()) {
                int fIdxN1 = findNeighbourIntersectionIndex(neighborOne, element);
                int fIdxN2 = findNeighbourIntersectionIndex(neighborTwo, element);

                intersectionMapGlobal_[eIdx][fIdx] = globalIntersectionIdx;
                intersectionMapGlobal_[eIdxN1][fIdxN1] = globalIntersectionIdx;
                fIdx++;

                intersectionMapGlobal_[eIdxN2][fIdxN2] = globalIntersectionIdx;
                fIdx++;

                setVelocityPositions(intersectionOne, globalIntersectionIdx, intersection->center());
                globalIntersectionIdx++;
            }
        } else {
            DUNE_THROW(Dune::InvalidStateException, "Refined-coarse intersection cannot be a boundary!");
        }
    }

//Should only be used for same level elements
    void setIndicesForIntersectionAndItsNeighbour(const Intersection& intersection,
                                                  const Element& element,
                                                  unsigned int eIdx,
                                                  unsigned int fIdx,
                                                  unsigned int& globalIntersectionIdx) {
        if (intersection.neighbor()) {
            auto neighbor = intersection.outside();
            int eIdxN = index(neighbor);
            if ((element.level() == neighbor.level() && eIdx < eIdxN)) {
                int fIdxN = findNeighbourIntersectionIndex(neighbor, element);
                intersectionMapGlobal_[eIdx][fIdx] = globalIntersectionIdx;
                intersectionMapGlobal_[eIdxN][fIdxN] = globalIntersectionIdx;
                setVelocityPositions(intersection, globalIntersectionIdx, intersection.geometry().center());
                globalIntersectionIdx++;
            } else {
                // std::cout<<"Already marked as neighbour"<<std::endl;
            }
        } else {
            intersectionMapGlobal_[eIdx][fIdx] = globalIntersectionIdx;
            setVelocityPositions(intersection, globalIntersectionIdx, intersection.geometry().center());
            globalIntersectionIdx++;
        }
    }

    int findNeighbourIntersectionIndex(const Element& neighbor, const Element& element) {
        int fIdxN = 0;
        //find intersection index of the neighbouring element
        for (const auto& intersectionNeighbor : intersections(gridView_, neighbor)) {
            if (intersectionNeighbor.neighbor()) {
                if (intersectionNeighbor.outside() == element) {
                    break;
                }
            }
            fIdxN++;
        }
        return fIdxN;
    }

    void setVelocityPositions(const Intersection& intersection, unsigned int globalIsIdx, const GlobalPosition& center) {
        unsigned int directionIdx = Dumux::directionIndex(std::move(intersection.centerUnitOuterNormal()));
        if (directionIdx == 0) {
            velocityXPositions_.push_back(std::make_pair(globalIsIdx, center));
        } else if (directionIdx == 1) {
            velocityYPositions_.push_back(std::make_pair(globalIsIdx, center));
        } else {
            DUNE_THROW(Dune::InvalidStateException, "Adaptive not prepated for three dimensions.");
        }
    }

    void setVelocityPositions(const Intersection& intersection, unsigned int globalIsIdx){
        setVelocityPositions(intersection, globalIsIdx, intersection.geometry().center());
    }

    unsigned int outerNormalIndex(const Intersection& is){
        unsigned int directionIndex = Dumux::directionIndex(std::move(is.centerUnitOuterNormal()));
        const auto& normal = is.centerUnitOuterNormal();
        bool directionPositive = normal[0]+normal[1] > 0.0;
        if(directionIndex == 0){
            if(directionPositive){
                return 1;
            } else {
                return 0;
            }
        } else if(directionIndex == 1){
            if(directionPositive){
                return 3;
            } else {
                return 2;
            }
        } else if(directionIndex == 2){
            DUNE_THROW(Dune::InvalidStateException, "Adaptive not prepared for three dimensions.");
        }
        return 0;//compiler warning ignore
    }

    GridView gridView_;
    const typename GridView::IndexSet* indexSet_;
    unsigned int numIntersections_;
    std::vector<std::unordered_map<int, int> > intersectionMapGlobal_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityXPositions_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityYPositions_;
    std::unordered_map<int, std::vector<CVDIntersectionBase<GridView, Intersection>*>> elementIntersections_;
};

} // end namespace Dumux

#endif
