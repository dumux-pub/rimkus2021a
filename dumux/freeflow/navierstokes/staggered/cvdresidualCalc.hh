// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesModel
 */

#ifndef DUMUX_CVD_NAVIERSTOKES_STAGGERED_RESIDUALCALC_HH
#define DUMUX_CVD_NAVIERSTOKES_STAGGERED_RESIDUALCALC_HH

#include <dumux/common/timeloop.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dumux/io/grid/cvdcontrolvolumegrids.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/cvdstaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/cvdffsubcontrolvolumeface.hh>
#include <dumux/freeflow/navierstokes/cvdAnalyticalSolutionIntegration.hh>

namespace Dumux{
    namespace Properties {
        template<class TypeTag, class MyTypeTag>
        struct CVGridGeometry { using type = UndefinedProperty; };
    }

template<class TypeTag>
class ResidualCalc
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using MLGTraits = typename CVDFreeFlowStaggeredDefaultScvfGeometryTraits<GridView>::template ScvfMLGTraits<Scalar> ;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using HostGridManager = GridManager<HostGrid>;
    using CVGridGeometry = GetPropType<TypeTag, Properties::CVGridGeometry>;

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using CellCenterSolutionVector = GetPropType<TypeTag, Properties::CellCenterSolutionVector>;
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Assembler = CVDStaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;

public:
    /*!
     * \brief The Constructor
     */
    ResidualCalc(std::shared_ptr<const Problem> problem)
    : problem_(problem)
    {}

    void calcResidualsStationary(std::shared_ptr<const GridGeometry> gridGeometry,
                                 CellCenterSolutionVector& cellCenterResidualWithAnalyticalSol,
                                 FaceSolutionVector& faceResidualWithAnalyticalSol) const
    {
        SolutionVector numericalResidual = calcNumericalResidualStationary_(gridGeometry);

        FaceSolutionVector areaWeightedFaceResidualWithAnalyticalSol = numericalResidual[GridGeometry::faceIdx()];
        std::cout << "velocity residual norm = " << areaWeightedFaceResidualWithAnalyticalSol.two_norm() << std::endl;
        faceResidualWithAnalyticalSol = areaWeightedToNonAreaWeightedFaceVec_(areaWeightedFaceResidualWithAnalyticalSol);

        CellCenterSolutionVector areaWeightedCCResidualWithAnalyticalSol = numericalResidual[GridGeometry::cellCenterIdx()];
        std::cout << "presssure residual norm = " << areaWeightedCCResidualWithAnalyticalSol.two_norm() << std::endl;
        cellCenterResidualWithAnalyticalSol = areaWeightedToNonAreaWeightedCCVec_(areaWeightedCCResidualWithAnalyticalSol);
    }

    void calcResidualsInstationary(std::shared_ptr<const TimeLoop<Scalar>> timeLoop,
                                   std::shared_ptr<const GridGeometry> gridGeometry,
                                   CellCenterSolutionVector& cellCenterResidualWithAnalyticalSol,
                                   FaceSolutionVector& faceResidualWithAnalyticalSol) const
    {
        Scalar t = timeLoop->time()+timeLoop->timeStepSize();
        Scalar tOld = timeLoop->time();

        SolutionVector numericalResidual = calcNumericalResidualInstationary_(gridGeometry, timeLoop, t, tOld);

        FaceSolutionVector areaWeightedFaceResidualWithAnalyticalSol = numericalResidual[GridGeometry::faceIdx()];
        std::cout << "velocity residual norm = " << areaWeightedFaceResidualWithAnalyticalSol.two_norm() << std::endl;
        faceResidualWithAnalyticalSol = areaWeightedToNonAreaWeightedFaceVec_(areaWeightedFaceResidualWithAnalyticalSol);

        CellCenterSolutionVector areaWeightedCCResidualWithAnalyticalSol = numericalResidual[GridGeometry::cellCenterIdx()];
        std::cout << "presssure residual norm = " << areaWeightedCCResidualWithAnalyticalSol.two_norm() << std::endl;
        cellCenterResidualWithAnalyticalSol = areaWeightedToNonAreaWeightedCCVec_(areaWeightedCCResidualWithAnalyticalSol);
    }

private:
    template <class Vec>
    auto areaWeightedToNonAreaWeightedFaceVec_(const Vec& areaWeightedVec) const
    {
        Dune::BlockVector<Dune::FieldVector<Scalar, 1>, std::allocator<Dune::FieldVector<Scalar, 1> > > resVel;
        resVel = areaWeightedVec;

        auto cvAreas = resVel;
        cvAreas = 0.;

        for (const auto& element : elements((*problem_).gridGeometry().gridView()))
        {
            auto fvGeometry = localView((*problem_).gridGeometry());
            fvGeometry.bindElement(element);
            for (const auto& scvf : scvfs(fvGeometry))
            {
                Scalar addToCVArea =  (scvf.area() * 0.5 * scvf.selfToOppositeDistance());
                if (scvf.isInsideFiner())
                    addToCVArea *= 0.5;

                cvAreas[scvf.dofIndex()] += addToCVArea;
            }
        }

        for (int i = 0; i < resVel.size(); ++i)
        {
            resVel[i] /= cvAreas[i];
        }

        return resVel;
    }

    template <class Vec>
    auto areaWeightedToNonAreaWeightedCCVec_(const Vec& areaWeightedVec) const
    {
        CellCenterSolutionVector resVec;
        resVec = areaWeightedVec;

        for (const auto& element : elements((*problem_).gridGeometry().gridView()))
            resVec[(*problem_).gridGeometry().gridView().indexSet().index(element)] /= element.geometry().volume();

        return resVec;
    }

    SolutionVector calcNumericalResidualInstationary_(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<const TimeLoop<Scalar>> timeLoop, Scalar t, Scalar tOld) const
    {
        const auto analyticalTPlusDeltaTTuple = problem_->getAnalyticalSolution(t);
        const auto analyticalTTuple = problem_->getAnalyticalSolution(tOld);

        SolutionVector analyticalTPlusDeltaT;
        analyticalTPlusDeltaT[GridGeometry::faceIdx()] = std::get<3>(analyticalTPlusDeltaTTuple);
        analyticalTPlusDeltaT[GridGeometry::cellCenterIdx()] = std::get<0>(analyticalTPlusDeltaTTuple);

        SolutionVector analyticalT;
        analyticalT[GridGeometry::faceIdx()] = std::get<3>(analyticalTTuple);
        analyticalT[GridGeometry::cellCenterIdx()] = std::get<0>(analyticalTTuple);

        auto localGridVariables = std::make_shared<GridVariables>(problem_, gridGeometry);
        localGridVariables->init(analyticalT);
        localGridVariables->update(analyticalTPlusDeltaT);

        const auto numDofsCellCenter = gridGeometry->numCellCenterDofs();
        const auto numDofsFace = gridGeometry->numIntersections();

        SolutionVector residual;
        residual[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
        residual[GridGeometry::faceIdx()].resize(numDofsFace);

        Assembler localAssemblerObject(problem_, gridGeometry, localGridVariables, timeLoop, analyticalT);
        localAssemblerObject.assembleResidual(residual, analyticalTPlusDeltaT);

        return residual;
    }

    SolutionVector calcNumericalResidualStationary_(std::shared_ptr<const GridGeometry> gridGeometry) const
    {
        const auto analyticalTuple = problem_->getAnalyticalSolution();

        SolutionVector analytical;
        analytical[GridGeometry::faceIdx()] = std::get<3>(analyticalTuple);
        analytical[GridGeometry::cellCenterIdx()] = std::get<0>(analyticalTuple);

        auto localGridVariables = std::make_shared<GridVariables>(problem_, gridGeometry);
        localGridVariables->init(analytical);

        const auto numDofsCellCenter = gridGeometry->numCellCenterDofs();
        const auto numDofsFace = gridGeometry->numIntersections();

        SolutionVector residual;
        residual[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
        residual[GridGeometry::faceIdx()].resize(numDofsFace);

        Assembler localAssemblerObject(problem_, gridGeometry, localGridVariables);
        localAssemblerObject.assembleResidual(residual, analytical);

        return residual;
    }

    std::shared_ptr<const Problem> problem_;
};
} // end namespace Dumux

#endif
