#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-appl/dumux-adaptivestaggered.git
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git

### Go to specific branches
cd dune-common && git checkout master && cd ..
cd dune-geometry && git checkout master && cd ..
cd dune-grid && git checkout master && cd ..
cd dune-istl && git checkout master && cd ..
cd dune-localfunctions && git checkout master && cd ..
cd dune-alugrid && git checkout master && cd ..
cd dumux-adaptivestaggered && git checkout next && cd ..
cd rimkus2021a && git checkout next && cd ..
cd dune-subgrid && git checkout master && cd ..
cd dumux && git checkout master && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
