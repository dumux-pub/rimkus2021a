SUMMARY
=======
This is the DuMuX module containing the code for producing the results for the Master's Thesis "Discretizing free flow coupled to porous-medium flow by a locally-refined finite-volume staggered-grid method using an interface with refined pressures and coarse velocities" of Arūnas Rimkus.

Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and clone this module:

```
mkdir New_Folder
cd New_Folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/rimkus2021a.git
```

After that, execute the file [installRimkus2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/rimkus2021a/-/blob/next/installRimkus2021a.sh)

```
cd rimkus2021a
git checkout next
cd ..
chmod u+x rimkus2021a/installRimkus2021a.sh
./rimkus2021a/installRimkus2021a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Applications
============


__Building from source__:

In order run the simulations navigate to the folder

```bash
cd rimkus2021a/build-cmake/appl/freeflow/navierstokes/donea
```

Compile the programm:

```bash
make test_ff_stokes_donea_vdP
```

And run by:

* ```./test_ff_stokes_donea_vdP params.input```
