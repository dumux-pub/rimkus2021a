add_input_file_links()

dune_add_test(NAME test_ff_navierstokes_angeli
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ${CMAKE_SOURCE_DIR}/../dumux/bin/testing/runtest.py
              CMD_ARGS       --script fuzzy
                             --files ${CMAKE_SOURCE_DIR}/../dumux/test/references/test_angeli-reference.vtu
                                     ${CMAKE_CURRENT_BINARY_DIR}/test_angeli-00045.vtu
                             --command "${CMAKE_CURRENT_BINARY_DIR}/test_ff_navierstokes_angeli params.input")
